package com.maybe.jvm;

/**
 * @author cdr
 * @version 1.0
 * @ClassName StringTableTest
 * @date 2022/12/16 14:14
 * @Description
 */
public class StringTableTest {

    /**
     * StringTable[]，常量池中的数组（串池）
     *
     * @param args
     */
    public static void main(String[] args) {
        //["a"]
        String s1 = "a";

        //["a","b"]
        String s2 = "b";

        //编译器在编译时会优化为 ab，拿着 ab 去 常量池中寻找数据，如果有则直接返回常量池数据
        //["a","b","ab"]
        String s4 = "a" + "b";

        //ab 拿着 ab 去 常量池中寻找数据
        String s5 = "ab";

        //new StringBuilder，存在堆中
        String s3 = s1 + s2;

        /**
         * TODO 主动的将字符串对象加入串池中
         * 1.如果串池中已存在该数据，则不会放入串池中（StringTable）。s4：new StringBuilder() 还是在堆内存中
         *
         * 2.如果串池中没有存在该数据，则引用串池中对象，并返回数据。 s4：StringTable[ "ab" ]
         */
        String s6 = s3.intern();

        //false
        System.out.println(s3 == s4);

        //true
        System.out.println(s4 == s5);

        /**
         * 1.目前这种执行顺序，s6 = s3.intern()，而串池中并没存在 s3数据
         * s3 = StringTable[ "ab" ]
         *
         * 转换
         * s5 == s6（StringTable[ "ab" ]）
         * true
         *
         * 2.如果将 s3代码 和 s4 对换执行顺序，s6 = s3.intern()，而串池中已经存在  s3 数据，
         * 并不会加入常量池中，并没有成功加入常量池。
         * s3 = new StringBuilder()
         *
         * TODO 但是这两种结果最后都会将常量池的数据返回给 s；1，2，两种情况只会改变s3自己的值
         *
         * 转换
         * s5 == s6（StringTable[ "ab" ]）
         * true
         */
        System.out.println(s5 == s6);

    }
}
