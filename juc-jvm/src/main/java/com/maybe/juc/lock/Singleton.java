package com.maybe.juc.lock;

import java.io.Serializable;

/**
 * TODO 懒汉单例，完整版
 */
public final class Singleton implements Serializable {
    /**
     * TODO init ：volatile 修饰，内部有读写内存屏障。保护关键字的可见性，防止指令重排导致幻读，脏读。
     * 写内存屏障：不会将 原本写屏障之前 的代码排在 写屏障之后！
     * 读内存屏障：不会将 原本读屏障之后 的代码排在 读屏障之前！
     */
    private static volatile Singleton init;

    private Singleton() {
        /**
         * 防止反射破坏单例模式
         */
        if (init != null) {
            throw new RuntimeException("No Destruction");
        }
    }

    /**
     * 防止反序列化破坏单例
     * TODO readResolve()，反序列化时它会先判断该类是否存在 readResolve 方法！
     * 有 readResolve 就会调用该方法，没有则会创建新对象
     *
     * @return
     */
    private Object readResolve() {
        return init;
    }


    /**
     * double-check，模式
     * TODO 提高性能，只有第一次初始化才会加 synchronized 锁
     *
     * @return
     */
    public static Singleton getInstance() {
        /**
         * TODO 不加 volatile：
         * 1，可见性问题，init 已经被初始化，由于没有在主存中读取，导致多次创建
         * 2，指令重排问题：
         * 在 init = new Singleton()时，可能会出现指令重排。
         * 先赋值一个 临时地址（还没有真正的初始化虚假引用地址），在进行对象的构造方法调用。这样会导致线程拿到一个错误的对象！！！
         */
        if (init == null) {
            //保证单例永远安全性
            synchronized (Singleton.class) {
                if (init == null) {
                    //创建完成后，第二次，将会被第一个 if 拦截不会进入 synchronized 提高性能
                    init = new Singleton();
                }
            }
        }
        return init;
    }


}
