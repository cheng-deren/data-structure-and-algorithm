package com.maybe.juc.lock;


import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 香烟案例
 * TODO 采用 可重入锁 优化
 */
@Slf4j
@SuppressWarnings(value = "ALL")
public class SmokeLockApplication {
    private static ReentrantLock room = new ReentrantLock();
    private static Boolean smoke = Boolean.FALSE;
    private static Boolean food = Boolean.FALSE;
    private static Condition smokeCondition = room.newCondition();
    private static Condition foodCondition = room.newCondition();

    public static void main(String[] args) throws InterruptedException {

        new Thread(() -> {
            room.lock();
            try {
                //错误唤醒，继续进行等待
                while (!smoke) {
                    log.debug("{}：没有烟，怎么干活啊！", Thread.currentThread().getName());
                    try {
                        smokeCondition.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (smoke) {
                    log.debug("{}：烟来了，开始干活！", Thread.currentThread().getName());
                } else {
                    log.debug("{}：没有烟，怎么干活啊！", Thread.currentThread().getName());
                }
            } finally {
                room.unlock();
            }
        }, "T1").start();

        new Thread(() -> {
            room.lock();
            try {
                //错误唤醒，继续进行等待
                while (!food) {
                    log.debug("{}：没有食物，怎么干活啊！", Thread.currentThread().getName());
                    try {
                        foodCondition.await();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (food) {
                    log.debug("{}：食物来了，开始干活！", Thread.currentThread().getName());
                } else {
                    log.debug("{}：没有食物，怎么干活啊！", Thread.currentThread().getName());
                }
            } finally {
                room.unlock();
            }
        }, "T2").start();

        for (int i = 1; i <= 3; i++) {
            String str = "Q" + i;
            new Thread(() -> {
                room.lock();
                try {
                    log.debug("{}：加油搬砖！", Thread.currentThread().getName());
                } finally {
                    room.unlock();
                }
            }, str).start();
        }

        TimeUnit.SECONDS.sleep(3);

        room.lock();
        try {
            food = true;
            log.debug("送食物的小弟，唤醒大哥。");
            foodCondition.signal();
        } finally {
            room.unlock();
        }
    }
}
