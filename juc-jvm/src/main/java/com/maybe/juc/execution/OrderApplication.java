package com.maybe.juc.execution;

import lombok.extern.slf4j.Slf4j;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Order
 * @date 2022/12/16 21:44
 * @Description 执行顺序，案例
 */
@Slf4j
public class OrderApplication {
    private static final int MAX_NUM = 100000000;

    public static void main(String[] args) throws InterruptedException {
        Room room = new Room();

        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < MAX_NUM; i++) {
                room.increment();
            }
        }, "t1");

        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < MAX_NUM; i++) {
                room.decrement();
            }
        }, "t2");

        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();
        log.info("最终计算结果：{}", room.getLogo());
    }

}


/**
 * 改成锁对象的方式，面向对象思想改进
 */
@Slf4j
class Room {
    private int logo = 0;


    public int getLogo() {
        synchronized (this) {
            return this.logo;
        }
    }

    public void increment() {
        synchronized (this) {
            logo++;
        }
    }

    public void decrement() {
        synchronized (this) {
            logo--;
        }
    }


    /**
     * TODO test1  ==  test1Method  这两个锁的都是 this 对象，两张方式等价
     */
    public void test1(){
        synchronized (this) {
            //业务代码
        }
    }
    public synchronized void test1Method(){
        //业务代码
    }


    /**
     * TODO  test2 == test2Method 这两个锁的都是 class 对象，两张方式等价
     */
    public static void test2(){
        synchronized (Room.class) {
            //业务代码
        }
    }
    public synchronized static void test2Method(){
            //业务代码
    }




}
