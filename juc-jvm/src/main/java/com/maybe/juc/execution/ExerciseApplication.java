package com.maybe.juc.execution;


import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Ex
 * @date 2022/12/19 20:13
 * @Description 卖票，案例
 */
public class ExerciseApplication {

    public static void main(String[] args) {
        ExerciseWindow window = new ExerciseWindow(1000);

        CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>();

        for (int i = 0; i < 5000; i++) {
            new Thread(() -> {
                //买票
                int method = window.method(new Random().nextInt(6));
                //计算总共成功买了多少票
                list.add(method);
            }).start();
        }

        System.out.println("总共买票：" + list.stream().mapToInt(Integer::intValue).sum());
        System.out.println("总剩余票数：" + window.getNum());
    }
}


/**
 * 买票窗口
 */
class ExerciseWindow {
    private int num;

    public ExerciseWindow(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    /**
     * 买票的方法，返回成功购买多少张票
     * TODO synchronized 锁当前对象（this）  num 是共享变量，涉及读写操作
     */
    public synchronized int method(int temp) {
        //TODO 多线程调用时，存在第一个线程还没更新 nun 而时间片用完。导致第二个线程会产生脏读
        if (num >= temp) {
            num -= temp;
            return temp;
        }
        return 0;
    }
}
