package com.maybe.juc.execution;

import java.util.Random;

/**
 * @author cdr
 * @version 1.0
 * @ClassName AccountTest
 * @date 2022/12/19 20:42
 * @Description
 */
public class AccountApplication {
    public static void main(String[] args) throws InterruptedException {
        Account a = new Account(100);
        Account b = new Account(100);

        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 50; i++) {
                a.transfer(b, new Random().nextInt(6));
            }
        });

        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 50; i++) {
                b.transfer(a, new Random().nextInt(6));
            }
        });

        thread1.start();
        thread2.start();
        thread1.join();
        thread2.join();

        System.out.println(a.getNum() + b.getNum());

        int[] tes = new int[5];
        System.out.println("====="+tes.length);
        System.out.println(tes[1] == tes[2]);

    }

}

/**
 * 账号类
 */
class Account {

    private int num;

    public Account(int num) {
        this.num = num;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }


    /**
     * @param item   对方账户
     * @param amount 转账金额
     * TODO 这里共享变量 this，item，两个对象都涉及到读写操作。虽然可以锁两个对象，但是容易造成死锁
     */
    public void transfer(Account item, int amount) {
        //TODO 这里锁住类对象，类对象只有一个。但并不是最优解，很影响性能
        synchronized (Account.class) {
            if (this.num >= amount) {
                this.setNum(this.getNum() - amount);
                item.setNum(item.getNum() + amount);
            }
        }
    }
}
