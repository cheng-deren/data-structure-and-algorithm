package com.maybe.juc.execution;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Cake {
    private String colo;
    private double money;

    @Override
    public String toString() {
        return "Cake{" +
                "colo='" + colo + '\'' +
                ", money=" + money +
                '}';
    }
}
