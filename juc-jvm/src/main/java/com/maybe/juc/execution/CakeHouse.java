package com.maybe.juc.execution;

import java.util.concurrent.TimeUnit;

public class CakeHouse implements Runnable {
    private Cake cake;
    private Thread thread1;
    private Thread thread2;
    @Override
    public void run() {
        cake = new Cake("金黄色", 50.00);
        thread1 = new Thread(() -> {
            System.out.println("顾客等蛋糕商人制作蛋糕");
            try {
                thread2.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("购买成功：" + cake.toString());
        }, "顾客");
        thread2 = new Thread(() -> {
            System.out.println("商人正在制作蛋糕");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("商人蛋糕制作完成");
        }, "商人");
    }
    public static void main(String[] args) {
        CakeHouse cakeHouse = new CakeHouse();
        cakeHouse.run();
        cakeHouse.thread1.start();
        cakeHouse.thread2.start();
    }
}
