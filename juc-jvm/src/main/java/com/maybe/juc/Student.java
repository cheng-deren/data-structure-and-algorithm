package com.maybe.juc;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName S
 * @date 2022/12/16 16:23
 * @Description
 */
@Data
@AllArgsConstructor
public class Student {
    private String id;
    private String name;



    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Objects.equals(id, student.id) && Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
