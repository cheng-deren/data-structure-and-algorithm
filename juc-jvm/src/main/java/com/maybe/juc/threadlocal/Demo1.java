package com.maybe.juc.threadlocal;

import com.google.common.util.concurrent.ThreadFactoryBuilder;

import java.util.concurrent.*;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Demo1
 * @date 2023/3/29 15:40
 * @Description TODO 线程池，不太好实现数据共享
 */
@SuppressWarnings(value = "ALL")
public class Demo1 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {

        ThreadLocal<String> local = new ThreadLocal<>();

        ThreadFactory nameFactory = new ThreadFactoryBuilder()
                .setNameFormat("thread1" + "-%d")
                .setDaemon(true).build();

        ThreadPoolExecutor pool = new ThreadPoolExecutor(
                5,
                10,
                1L,
                TimeUnit.SECONDS,
                new ArrayBlockingQueue<>(10),
                nameFactory,
                new ThreadPoolExecutor.CallerRunsPolicy()
        );

        Future<String> submit1 = pool.submit(() -> {
            for (int i = 1; i <= 500; i++) {
                local.set("------A" + i + "A------");
                System.out.println(Thread.currentThread().getName() + "存入数据：" + local.get());
            }
            return "取出数据：" + local.get();
        });


        TimeUnit.SECONDS.sleep(1);

        Future<?> submit2 = pool.submit(() -> {
            for (int i = 1; i <= 500; i++) {
                System.out.println(Thread.currentThread().getName() + "取出数据：" + local.get());
            }
            return "取出数据：" + local.get();
        });

        System.out.println(submit1.get());
        System.out.println(submit2.get());
    }
}
