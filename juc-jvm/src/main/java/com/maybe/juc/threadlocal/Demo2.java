package com.maybe.juc.threadlocal;

import java.io.IOException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Demo1
 * @date 2023/3/29 15:40
 * @Description
 */
@SuppressWarnings(value = "ALL")
public class Demo2 {
    public static void main(String[] args) throws ExecutionException, InterruptedException, IOException {
        Share share = new Share();
//        ThreadShare share = new ThreadShare();

        for (int i = 1; i <= 50000; i++) {
            Thread thread = new Thread(() -> {
                String name = Thread.currentThread().getName();
                share.set(name);
                System.out.println("存入数据：" + name + "  >>>>> " + "  取出数据：" + share.get());
            });
            thread.start();
        }
        System.in.read();
    }

    /**
     * 非线程安全共享类
     */
    static class Share {
        private String item;

        public String get() {
            return item;
        }

        public void set(String item) {
            this.item = item;
        }
    }

    /**
     * 线程安全共享类，采用 ThreadLocal 修饰
     */
    static class ThreadShare {
        ThreadLocal<String> local = new ThreadLocal<String>();
        private String item;

        public String get() {
            return local.get();
        }

        public void set(String item) {
            local.set(item);
        }
    }
}
