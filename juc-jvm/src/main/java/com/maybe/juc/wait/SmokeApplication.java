package com.maybe.juc.wait;


import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 香烟案例
 * TODO 多个线程同步等待时：建议使用 notifyAll() + while 搭配使用
 */
@Slf4j
public class SmokeApplication {
    private static final Object ROOM = new Object();
    private static Boolean smoke = Boolean.FALSE;
    private static Boolean food = Boolean.FALSE;

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            synchronized (ROOM) {
                try {
                    //错误唤醒，继续进行等待
                    while (!smoke) {
                        log.debug("{}：没有烟，怎么干活啊！", Thread.currentThread().getName());
                        ROOM.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (smoke) {
                    log.debug("{}：烟来了，开始干活！", Thread.currentThread().getName());
                } else {
                    log.debug("{}：没有烟，怎么干活啊！", Thread.currentThread().getName());
                }
            }
        }, "T1").start();

        new Thread(() -> {
            synchronized (ROOM) {
                try {
                    //错误唤醒，继续进行等待
                    while (!food) {
                        log.debug("{}：没有食物，怎么干活啊！", Thread.currentThread().getName());
                        ROOM.wait();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (food) {
                    log.debug("{}：食物来了，开始干活！", Thread.currentThread().getName());
                } else {
                    log.debug("{}：没有食物，怎么干活啊！", Thread.currentThread().getName());
                }
            }
        }, "T2").start();

        for (int i = 1; i <= 5; i++) {
            String str = "Q" + i;
            new Thread(() -> {
                synchronized (ROOM) {
                    log.debug("{}：加油搬砖！", Thread.currentThread().getName());
                }
            }, str).start();
        }

        TimeUnit.SECONDS.sleep(3);
        synchronized (ROOM) {
            food = true;
            log.debug("送食物的小弟，唤醒大哥。");
            /**
             * TODO notify() : 随机唤醒等待线程 由于，目前 T1，T2，都在等待这样唤醒会导致错误唤醒的现象。
             * TODO 多个线程同步等待时：建议使用 notifyAll() + while 搭配使用
             */
            ROOM.notifyAll();
        }

    }
}
