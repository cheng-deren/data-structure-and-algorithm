package com.maybe.juc.wait;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 消息队列模式，案例  一人生产一人消费
 */
@Slf4j
public class QueueApplication {
    public static void main(String[] args) throws InterruptedException {
        Guarded guarded = new Guarded();
        String str1 = "consume";
        new Thread(() -> {
            synchronized (guarded) {
                log.debug("消费：{}", guarded.getResponse(1));
            }
        }, str1).start();

        /**
         * 这里要保证生产在消费的后面执行，因为生产拿到锁会 sleep 3 秒 ！！！
         * 而 sleep 是抱着锁睡觉的，会导致消费者拿不到锁，从而消费者的等待超时无效。
         */
        TimeUnit.SECONDS.sleep(1);

        String str2 = "production";
        new Thread(() -> {
            synchronized (guarded) {
                try {
                    //模拟生产数据
                    TimeUnit.SECONDS.sleep(3);
                    guarded.setResponse(str1);
                    log.debug("生产：{}", str1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }, str2).start();

    }
}

/**
 * 消息对象
 */
@Slf4j
class Guarded {
    private Object response;

    /**
     * 获取消息，等待生产
     *
     * @return
     */
    public Object getResponse() {
        synchronized (this) {
            while (response == null) {
                //数据未生产，继续等待
                try {
                    log.debug("等待数据生产中...");
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            return response;
        }
    }


    /**
     * 获取消息，等待生产（有等待时间）
     * TODO 借鉴 join() 源码实现，join也是采用  保护性暂停  模式
     *
     * @return
     */
    public Object getResponse(long time) {
        if (time <= 0) {
            throw new RuntimeException();
        }
        synchronized (this) {
            long start = System.currentTimeMillis();
            long difference = 0;
            while (response == null) {
                //这一轮循环，应该等待的时间。
                long item = time - difference;
                if (item <= 0) {
                    break;
                }
                try {
                    log.debug("等待数据生产中...");
                    this.wait(item);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                difference = System.currentTimeMillis() - start;
            }
            log.debug("response: {}", response);
            return response;
        }

    }

    /**
     * 生产消息
     *
     * @param response
     */
    public void setResponse(Object response) {
        synchronized (this) {
            this.response = response;
            //消息生产成功，唤醒等待线程。
            this.notifyAll();
        }
    }
}
