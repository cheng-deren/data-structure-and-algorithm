package com.maybe.juc.wait;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * 线程唤醒与等待
 */
@Slf4j
public class Test1 {
    private static final Object LOCK = new Object();

    public static void main(String[] args) throws InterruptedException {
        new Thread(() -> {
            synchronized (LOCK) {
                log.debug("{}等待", Thread.currentThread().getName());
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.debug("{}唤醒成功，其他代码。", Thread.currentThread().getName());
            }
        }, "T1").start();
        new Thread(() -> {
            synchronized (LOCK) {
                log.debug("{}等待", Thread.currentThread().getName());
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.debug("{}唤醒成功，其他代码。", Thread.currentThread().getName());
            }
        }, "T2").start();
        new Thread(() -> {
            synchronized (LOCK) {
                log.debug("{}等待", Thread.currentThread().getName());
                try {
                    LOCK.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                log.debug("{}唤醒成功，其他代码。", Thread.currentThread().getName());
            }
        }, "T3").start();

        TimeUnit.SECONDS.sleep(1);
        synchronized (LOCK) {
            /**
             * notify(): 随机唤醒一个线程（T2唤醒成功，其他代码）
             * notifyAll(): 唤醒全部处于等待状态下的线程（T1，T2，T3唤醒成功，其他代码）
             */
            LOCK.notify();
            LOCK.notifyAll();
        }
    }
}
