package com.maybe.juc.creation;

import java.util.StringJoiner;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Test1
 * @date 2023/3/29 15:24
 * @Description 字符串转大小写
 */
@SuppressWarnings(value = "ALL")
public class Test1 {
    public static void main(String[] args) {
        String str = " A_this is_name,zhang-san a_ - ,";
        char[] chars = str.toCharArray();
        String field = null;
        StringJoiner joiner = new StringJoiner("");
        for (int i = 0; i < chars.length; i++) {
            String item = Character.toString(chars[i]);
            if (item.contains(" ") || item.contains("_") || item.contains(",") || item.contains("-")) {
                if (++i >= chars.length) {
                    break;
                }
                chars[i] = Character.toUpperCase(chars[i]);
            }
            field = Character.toString(chars[i]);
            if (joiner.length() == 0) {
                field = field.toLowerCase();
            }
            joiner.add(field);
        }
        System.out.println(joiner);
    }
}