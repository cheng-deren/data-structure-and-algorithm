package com.maybe.juc.creation;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @author cdr
 * @version 1.0
 * @ClassName OverallPlanningApplication
 * @date 2022/12/16 21:07
 * @Description TODO 统筹之烧水泡茶，案例
 */
@Slf4j
public class OverallPlanningApplication {

    public static void main(String[] args) throws InterruptedException {
        Thread thread1 = new Thread(() -> {
            try {
                log.info("洗水壶");
                TimeUnit.SECONDS.sleep(1);
                log.info("烧开水");
                TimeUnit.SECONDS.sleep(5);
                log.info("A线程运行结束");
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }, "A");
        Thread thread2 = new Thread(() -> {
            try {
                log.info("洗茶壶");
                TimeUnit.SECONDS.sleep(1);
                log.info("洗茶杯");
                TimeUnit.SECONDS.sleep(1);
                log.info("拿茶叶");
                TimeUnit.SECONDS.sleep(1);
                thread1.join();
                log.info("一切准备就绪，开始泡茶喝");
                log.info("B线程运行结束");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }, "B");

        long start = System.currentTimeMillis();
        thread1.start();
        thread2.start();
        thread2.join();
        long end = System.currentTimeMillis();
        log.info("总耗时：{} 秒", (end - start) / 1000);

    }

}
