package com.maybe.juc.creation;

import lombok.extern.slf4j.Slf4j;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Test1
 * @date 2022/12/16 17:21
 * @Description 多线程的执行顺序，交替执行
 */
@Slf4j
public class WaitTest1 {
    public static void main(String[] args) {

        Object o = new Object();
        Runnable runnable = () -> {
            synchronized (o) {
                try {
                    o.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };

        //对象唤醒
        synchronized (o) {
            o.notify();
            o.notifyAll();
        }

        Thread thread = new Thread(runnable);
        //线程唤醒
        thread.notify();
    }
}
