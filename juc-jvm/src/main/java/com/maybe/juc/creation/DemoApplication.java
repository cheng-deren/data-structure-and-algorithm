package com.maybe.juc.creation;

/**
 * @author cdr
 * @version 1.0
 * @ClassName DemoApplication
 * @date 2023/1/4 14:43
 * @Description
 */
public class DemoApplication {
    public static void main(String[] args) {
        DemoApplicationTest test = new DemoApplicationTest();
        test.start();
    }

    static class DemoApplicationTest extends Thread {
        @Override
        public void run() {
            for (int i = 0; i < 2; i++) {
                new Thread(() -> {
                    System.out.println(sum());
                }).start();
            }
        }
        public int sum() {
            int temp = 0;
            for (int i = 800; i <= 5000; i++) {
                temp += i;
            }
            return temp;
        }

    }
}
