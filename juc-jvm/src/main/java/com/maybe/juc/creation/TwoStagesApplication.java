package com.maybe.juc.creation;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @author cdr
 * @version 1.0
 * @ClassName TwoStages
 * @date 2022/12/16 20:05
 * @Description TODO 两阶段终止模式，案例
 */
@Slf4j
public class TwoStagesApplication {
    public static void main(String[] args) throws InterruptedException {
        Monitor monitor = new Monitor();
        monitor.start();
        Thread.sleep(3000);
        monitor.stop();

    }

}


/**
 * 监控类
 */
@Slf4j
@SuppressWarnings(value = "ALL")
class Monitor {
    /**
     * 线程监控
     */
    private Thread thread;

    /**
     * 启动监控
     */
    public void start() {
        thread = new Thread(() -> {
            Thread current = Thread.currentThread();
            while (true) {
                /**
                 * 判断是否被打断
                 * TODO interrupted() ： 判断线程是否被打断，会清空标记
                 * TODO isInterrupted() ：判断线程是否被打断，不会清空标记
                 */
                if (current.isInterrupted()) {
                    log.info("料理后事");
                    break;
                }
                try {
                    //sleep中被打断会抛出异常并且重置标记为 false
                    TimeUnit.SECONDS.sleep(1);
                    log.info("执行监控记录");
                } catch (InterruptedException e) {
                    e.printStackTrace();
                    //TODO 重置sleep被打断标记，没有重置标记将无法正常停止线程
                    current.interrupt();
                    log.info("sleep interrupted");
                }
            }
        });
        thread.start();
    }
    /**
     * 打断线程
     * TODO interrupt()：打断线程，正常情况打断线程设置打断标记： true；  sleep，wait，join，状态下的线程被打断，将会重置标记 false
     */
    public void stop() {
        thread.interrupt();
    }


}

