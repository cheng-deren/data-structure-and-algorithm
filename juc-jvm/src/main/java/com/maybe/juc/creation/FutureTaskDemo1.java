package com.maybe.juc.creation;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

/**
 * @author cdr
 * @version 1.0
 * @ClassName FutureTaskDemo1
 * @date 2022/12/16 17:03
 * @Description
 */
@Slf4j
public class FutureTaskDemo1 {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureTask<Integer> task = new FutureTask<Integer>(new Callable<Integer>() {
            @Override
            public Integer call() throws Exception {
                log.info("task");
                Thread.sleep(1000);
                return 999;
            }
        });

        Thread thread = new Thread(task);
        thread.start();
        log.info("{}",task.get());

    }
}
