package com.maybe.juc.creation;

import lombok.extern.slf4j.Slf4j;

/**
 * @author cdr
 * @version 1.0
 * @ClassName RunnableDemo1
 * @date 2022/12/16 16:03
 * @Description
 */
@Slf4j
public class RunnableDemo1 {
    public static void main(String[] args) {
        Runnable runnable1 = new Runnable() {
            @Override
            public void run() {
                log.info("runnable1");
            }
        };

        Runnable runnable2 = () -> {
            log.info("runnable2");
        };

        Thread thread1 = new Thread(runnable1);
        Thread thread2 = new Thread(runnable2);
        thread1.start();
        thread2.start();
    }
}
