package com.maybe.juc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Test
 * @date 2022/12/16 14:57
 * @Description
 */
public class Test {
    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<>();
        Student student1 = new Student("1", "张三");
        Student student2 = new Student("1", "李四");
        Collections.addAll(list, student1, student2);
        Map<String, Student> map = list.parallelStream().collect(Collectors.toMap(Student::getId, Function.identity(), (v1, v2) -> v2));
        System.out.println(map);
    }
}
