package com.maybe.juc.pool;

import lombok.extern.slf4j.Slf4j;

import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicIntegerArray;

/**
 * @author cdr
 * @version 1.0
 * @ClassName PoolApplication
 * @date 2022/12/19 20:42
 * @Description 线程池创建练习，参考 tomcat 连接池实现
 */
@Slf4j
@SuppressWarnings(value = "ALL")
public class PoolApplication {
    public static void main(String[] args) throws InterruptedException {
        Pool pool = new Pool(2);
        for (int i = 1; i <= 3; i++) {
            String str = "Q" + i;
            new Thread(() -> {
                Collection borrow = pool.borrow();
                try {
                    //模拟使用线程时间
                    TimeUnit.SECONDS.sleep(5);
                    //归还线程
                    pool.free(borrow);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }, str).start();
        }


    }

}

/**
 * 线程池
 */
@Slf4j
class Pool {
    /**
     * 线程池 大小
     */
    private final int poolSize;
    /**
     * 连接对象
     */
    private Collection[] collectionList;
    /**
     * 线程状态；0：空闲  1：繁忙，默认全是 0
     */
    private AtomicIntegerArray statusList;

    public Pool(int poolSize) {
        this.poolSize = poolSize;
        this.collectionList = new Collection[poolSize];
        this.statusList = new AtomicIntegerArray(new int[poolSize]);
        for (int i = 0; i < poolSize; i++) {
            collectionList[i] = new DiyCollection();
        }
    }


    /**
     * 借出连接
     *
     * @return Collection
     */
    public Collection borrow() {
        while (true) {
            long start = System.currentTimeMillis();
            for (int i = 0; i < poolSize; i++) {
                //判断是否有空闲连接
                if (statusList.get(i) == 0) {
                    /**
                     * TODO 使用 CAS 保证修改数据安全
                     * TODO 借出连接；修改连接状态，这里借出 collection 对象存在多线程竞争的情况
                     */
                    if (statusList.compareAndSet(i, 0, 1)) {
                        log.debug("borrow{}", Thread.currentThread().getName());
                        return collectionList[i];
                    }
                }
            }
            /**
             * 遍历完发现并没有空闲连接则进入阻塞状态
             */
            synchronized (this) {
                try {
                    log.debug("wait...");
                    this.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            /**
             * 等待超时
             */
            long end = System.currentTimeMillis();
            if (end - start > 3000) {
                throw new RuntimeException("time out");
            }
        }

    }


    /**
     * 归还连接
     *
     * @return Collection
     */
    public void free(Collection collection) {
        if (collection == null) {
            return;
        }
        for (int i = 0; i < poolSize; i++) {
            //找到归还对象
            if (collectionList[i] == collection) {
                log.debug("free{}", Thread.currentThread().getName());

                //TODO 归还连接；修改连接状态，这里归还 collection 对象的只有一个线程则不需要考虑线程安全
                statusList.set(i, 0);
                //唤醒阻塞线程
                synchronized (this) {
                    log.debug("notifyAll...");
                    this.notifyAll();
                }
            }
        }

    }


}

/**
 * 临时连接对象
 */
@Slf4j
class DiyCollection implements Collection {

    public DiyCollection() {
        log.debug("DiyCollection init{}", Thread.currentThread().getName());
    }

    @Override
    public String toString() {
        return "DiyCollection{}";
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public boolean isEmpty() {
        return false;
    }

    @Override
    public boolean contains(Object o) {
        return false;
    }

    @Override
    public Iterator iterator() {
        return null;
    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public boolean add(Object o) {
        return false;
    }

    @Override
    public boolean remove(Object o) {
        return false;
    }

    @Override
    public boolean addAll(Collection c) {
        return false;
    }

    @Override
    public void clear() {

    }

    @Override
    public boolean retainAll(Collection c) {
        return false;
    }

    @Override
    public boolean removeAll(Collection c) {
        return false;
    }

    @Override
    public boolean containsAll(Collection c) {
        return false;
    }

    @Override
    public Object[] toArray(Object[] a) {
        return new Object[0];
    }
}
