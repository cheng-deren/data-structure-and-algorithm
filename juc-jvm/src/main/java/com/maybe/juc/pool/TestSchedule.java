package com.maybe.juc.pool;

import lombok.extern.slf4j.Slf4j;

import java.time.DayOfWeek;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @author cdr
 * @version 1.0
 * @ClassName TestSchedule
 * @date 2023/1/4 18:50
 * @Description 自定义实现定时任务，每周三  19点执行任务
 */
@Slf4j
@SuppressWarnings(value = "ALL")
public class TestSchedule {
    public static void main(String[] args) {
        LocalDateTime now = LocalDateTime.now();
        //时间重置到 19点
        LocalDateTime tempTime = now.withHour(19).withMinute(4).withSecond(0).withNano(0);
        //修改到周三
        LocalDateTime dateTime = tempTime.with(DayOfWeek.WEDNESDAY);
        //判断当前时间是否大于周三，大于则加一周
        if (now.compareTo(dateTime) > 0) {
            //加一周
            dateTime = dateTime.plusWeeks(1);
        }

        ScheduledExecutorService pool = Executors.newScheduledThreadPool(1);
        long initialDelay = Duration.between(now, dateTime).toMillis();
        long period = 1000 * 60 * 60 * 24 * 7;

        /**
         * Runnable
         * initialDelay     时间差
         * period           间隔时间
         * TimeUnit         单位
         */
        pool.scheduleAtFixedRate(() -> {
            log.debug("task...");
        }, initialDelay, period, TimeUnit.MILLISECONDS);





    }
}
