package com.maybe.juc.pool;

import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;

/**
 * @author cdr
 * @version 1.0
 * @ClassName DemoFixedThreadPool
 * @date 2023/1/4 16:22
 * @Description
 */
@Slf4j
@SuppressWarnings(value = "ALL")
public class DemoFixedThreadPool {
    public static void main(String[] args) throws ExecutionException, InterruptedException {
        ExecutorService pool = Executors.newFixedThreadPool(3);
        log.debug("测试 返回结果是 Future");
        Future<?> future = submit(pool);
        log.debug("带一个返回结果 future：{}", future.get());
        log.debug("------------------------------------");

        log.debug("测试 返回结果是 List<Future>");
        invokeAll(pool);
        log.debug("------------------------------------");

        log.debug("多个任务取最先执行完成的一个返回结果，剩下的任务不会继续执行!!!");
        invokeAny(pool);

        pool.shutdown();

    }


    /**
     * 多个任务取最先执行完成的一个返回结果，剩下的任务不会继续执行!!!
     *
     * @param pool
     * @throws InterruptedException
     */
    private static void invokeAny(ExecutorService pool) throws InterruptedException, ExecutionException {
        String invokeAny = pool.invokeAny(Arrays.asList(
                () -> {
                    TimeUnit.SECONDS.sleep(10);
                    log.debug("invokeAll：1");
                    return "result 1";
                },
                () -> {
                    TimeUnit.SECONDS.sleep(20);
                    log.debug("invokeAll：2");
                    return "result 2";
                },
                () -> {
                    TimeUnit.SECONDS.sleep(3);
                    log.debug("invokeAll：3");
                    return "result 3";
                }
        ));

        log.debug("invokeAny !!!：{}", invokeAny);

    }

    /**
     * 返回结果是 List<Future>
     *
     * @param pool
     * @throws InterruptedException
     */
    private static void invokeAll(ExecutorService pool) throws InterruptedException {
        List<Future<String>> futureList = pool.invokeAll(Arrays.asList(
                () -> {
                    log.debug("invokeAll：1");
                    TimeUnit.SECONDS.sleep(1);
                    return "result 1";
                },
                () -> {
                    log.debug("invokeAll：2");
                    TimeUnit.SECONDS.sleep(2);
                    return "result 2";
                },
                () -> {
                    log.debug("invokeAll：3");
                    TimeUnit.SECONDS.sleep(3);
                    return "result 3";
                }
        ));

        futureList.forEach(item -> {
            try {
                log.debug("带集合的返回结果 List<Future>：{}", item.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            } catch (ExecutionException e) {
                e.printStackTrace();
            }
        });
    }

    /**
     * 返回结果是 Future
     *
     * @param threadPool
     * @return
     */
    private static Future<?> submit(ExecutorService threadPool) {
        Future<?> future = threadPool.submit(() -> {
            log.debug("submit");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return "ok";
        });
        return future;
    }
}
