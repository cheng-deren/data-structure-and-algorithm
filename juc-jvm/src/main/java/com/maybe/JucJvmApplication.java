package com.maybe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author cdr
 * @version 1.0
 * @ClassName JucJvmApplication
 * @date 2022/12/16 15:45
 * @Description
 */
@SpringBootApplication
public class JucJvmApplication {
    public static void main(String[] args) {
        SpringApplication.run(JucJvmApplication.class, args);
    }
}
