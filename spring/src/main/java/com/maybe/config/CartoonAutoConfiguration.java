package com.maybe.config;

import com.maybe.entry.Cat;
import lombok.Data;
import com.maybe.entry.Mouse;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

import java.util.Objects;


/**
 * @author cdr
 * @version 1.0
 * @ClassName Cartoon
 * @date 2023/2/3 10:29
 * @Description
 */
@Data
@ConditionalOnClass(name = "com.mysql.jdbc.Driver")
@EnableConfigurationProperties(CartoonProperties.class)
public class CartoonAutoConfiguration {
    private Cat cat;
    private Mouse mouse;
    private CartoonProperties cartoonProperties;

    public CartoonAutoConfiguration(CartoonProperties properties) {
        this.cartoonProperties = properties;

        this.cat = new Cat("猫",1);
        if (Objects.nonNull(properties.getCat())) {
            if (properties.getCat().getName() != null && properties.getCat().getName() != "") {
                cat.setName(properties.getCat().getName());
            }
            if (properties.getCat().getAge() != null) {
                cat.setAge(properties.getCat().getAge());
            }
        }

        this.mouse = new Mouse("老鼠",2);
        if (Objects.nonNull(properties.getMouse())) {
            if (properties.getMouse().getName() != null && properties.getMouse().getName() != "") {
                mouse.setName(properties.getMouse().getName());
            }
            if (properties.getMouse().getAge() != null) {
                mouse.setAge(properties.getMouse().getAge());
            }
        }
    }

    public void play() {
        System.out.println(cat.toString());
        System.out.println(mouse.toString());
    }


}
