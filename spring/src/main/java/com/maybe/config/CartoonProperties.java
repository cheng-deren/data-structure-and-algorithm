package com.maybe.config;

import com.maybe.entry.Cat;
import com.maybe.entry.Mouse;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Cartoon
 * @date 2023/2/3 10:29
 * @Description
 */

@Data
@ConfigurationProperties(prefix = "cartoon")
public class CartoonProperties {
    private Cat cat;
    private Mouse mouse;
}
