package com.maybe.entry;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Cat
 * @date 2023/2/3 10:11
 * @Description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Cat {
    private String name;
    private Integer age;
}
