package com.maybe.entry;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Cat
 * @date 2023/2/3 10:11
 * @Description
 */
@Data
public class Dog {
    private String name;
    private Integer age;
}
