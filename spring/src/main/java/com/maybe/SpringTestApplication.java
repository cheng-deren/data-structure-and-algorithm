package com.maybe;

import com.maybe.config.CartoonAutoConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * @author cdr
 * @version 1.0
 * @ClassName SpringTestApplication
 * @date 2023/2/3 9:52
 * @Description
 */
@SpringBootApplication
public class SpringTestApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(SpringTestApplication.class, args);
        CartoonAutoConfiguration bean = context.getBean(CartoonAutoConfiguration.class);
        System.out.println("-----------------------------------------------");
        bean.play();
        System.out.println("-----------------------------------------------");
    }
}
