package com.maybe.递归;


import java.math.BigDecimal;
import java.math.BigInteger;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Factorial
 * @date 2022/11/3 15:01
 * @Description 递归初识
 */
public class Factorial1 {
    public static void main(String[] args) {

        System.out.println("最优解：" + factorialMethodExcellent(100));

        System.out.println("递归写法：" + factorialMethod1(100));
        System.out.println("常规写法：" + factorialMethod2(100));

    }


    /**
     * 阶乘最优解
     */
    public static BigInteger factorialMethodExcellent(int param) {
        final BigInteger zero = new BigInteger("0");
        final BigInteger one = new BigInteger("1");

        BigInteger bigParam = new BigInteger(String.valueOf(param));
        BigInteger result = new BigInteger("1");

        while (bigParam.compareTo(zero) > 0) {
            result = result.multiply(bigParam);
            bigParam = bigParam.subtract(one);
        }
        return result;
    }


    /**
     * long ： 9223372036854775807 （-2^64 到 2^64 -1）
     * n > 25 long 就无法接受，数据类型溢出
     *
     * @param n
     * @return
     */
    private static long factorialMethod1(long n) {
        if (n == 1) {
            return 1;
        }
        return n * factorialMethod1(n - 1);
    }

    /**
     * 可以处理大数据且不会栈溢出
     *
     * @param n
     * @return
     */
    private static BigDecimal factorialMethod2(int n) {
        BigDecimal bigDecimal = new BigDecimal(n);
        for (int i = 1; i < n; i++) {
            bigDecimal = bigDecimal.multiply(new BigDecimal(i));
        }
        return bigDecimal;
    }


}
