//在提交之前执行进行验证返回验证结果信息{success:true,msg:""}
var _beforeSubmit=async function(){
    var self = this;
    if (self.data.ASSESSMENT_TEMPLATE_TYPE == '2' && self.func_isEmpty(self.data.ASSESSMENT_TEMPLATE_DEPT_TYPE)) {
        return {success:false,msg:"主表【绩效书部门类型】必填"};
    }
    if (self.data.ALLOWED_USE_SCOPE == '0' && self.func_isEmpty(self.data.ALLOWED_USE_DEPT)) {
        return {success:false,msg:"主表【允许使用的部门】必填"};
    }

    debugger;
    let index = 1;
    let formList = self.data.sub__W_ASSESSMENT_TEMPLATE_INDEX_DETAIL;
    for (let i = 0; i < formList.length; i++) {
        let item = formList[i];

        //DATA_SOURCE ：数据来源 1：模板引用  2：指标库  3：部门绩效书  4：新增指标
        if (item.DATA_SOURCE == '4'){
            if (!item.INDEX_TYPE){
                return {success:false,msg:"子表 绩效考核书模板指标明细 字段【指标类型】必填 (第" + index + "行)"};
            }
            if (!item.INDEX_NAME){
                return {success:false,msg:"子表 绩效考核书模板指标明细 字段【指标名称】必填 (第" + index + "行)"};
            }
            if (!item.INDEX_ATTR){
                return {success:false,msg:"子表 绩效考核书模板指标明细 字段【指标属性】必填 (第" + index + "行)"};
            }
            if (!item.INDEX_EVAL_TYPE){
                return {success:false,msg:"子表 绩效考核书模板指标明细 字段【指标评价类型】必填 (第" + index + "行)"};
            }
            if (!item.INDEX_DEFIN){
                return {success:false,msg:"子表 绩效考核书模板指标明细 字段【指标定义】必填 (第" + index + "行)"};
            }
            if (!item.INDEX_DESCRIPTION){
                return {success:false,msg:"子表 绩效考核书模板指标明细 字段【指标说明】必填 (第" + index + "行)"};
            }
            if (!item.INDEX_EVALUATE_CALCULATION){
                return {success:false,msg:"子表 绩效考核书模板指标明细 字段【指标评定/计算方式】必填 (第" + index + "行)"};
            }
        }

        //“指标属性” 等于 “量化指标（有目标值、有权重）” 禁用 目标值，单位，
        if(self.func_isEmpty(item.INDEX_ATTR)){
            let indexAttr = JSON.parse(item.INDEX_ATTR);
            if (indexAttr.value == '1' && item.DATA_SOURCE != '3') {
                if (!item.TARGET_VALUE){
                    return {success:false,msg:"子表 绩效考核书模板指标明细 字段【目标值】必填 (第" + index + "行)"};
                }
                if (!item.UNIT){
                    return {success:false,msg:"子表 绩效考核书模板指标明细 字段【单位】必填 (第" + index + "行)"};
                }
            }
        }

        //校验权重，是否超过 100%
        if (item.WEIGHT != 100.00){
            return {success:false,msg:"所有指标的权重之和必须等于100%！"};
        }
        index++;
    }


    //模版确认状态：每次保存重置为编辑中  1：编辑中  2：模板编制完成
    self.data.EDIT_STATUS = '1';

    return {success:true,msg:""};
}