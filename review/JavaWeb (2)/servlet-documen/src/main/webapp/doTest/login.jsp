<style>
    #container {
        width: 1000px;
        margin: 0 auto;
    }
    #left {
        width: 500px;
        height: 400px;
        background-color: black;
        float: left;
    }

    #middle {
        width: 500px;
        height: 400px;
        background-color: white;
        float: left;
    }

    #right {
        width: 500px;
        height: 400px;
        background-color: grey;
        float: left;
    }

</style>
<body>
<div id="container">
    <div id="left"></div>
    <div id="middle"></div>
    <div id="right"></div>
</div>
</body>