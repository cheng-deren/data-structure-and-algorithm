<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="javax.print.DocFlavor" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>

<table>
    <p>学生信息如下：</p>
    <tr>
        <th>用户名</th>
        <th>密&emsp;码</th>
    </tr>

    <%
        Class.forName("com.mysql.jdbc.Driver");
        String url = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8";
        String username = "root";
        String password = "root";
        Connection connection = DriverManager.getConnection(url, username, password);
        String sql = "select * from user";
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        while (resultSet.next()) {

    %>
    <tr>
        <td><%=resultSet.getString("USER_NAME")%>
        </td>
        <td><%=resultSet.getString("USER_PASSWORD")%>
        </td>
    </tr>

    <%
        }
    %>
    <br/>
</table>

</body>

</body>
</html>
