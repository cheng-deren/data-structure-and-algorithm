package com.company.service;

import com.company.pojo.User;

import java.sql.SQLException;

/**
 * @author cdr
 * @version 1.0
 * @ClassName UserService
 * @date 2022/6/16 20:04
 * @Description
 */
public interface UserService {

    public User queryByNp(String n, String p) throws SQLException;
}
