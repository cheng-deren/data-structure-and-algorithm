package com.company.dao;

import com.company.pojo.User;
import com.company.utils.JdbcUtils;

import javax.print.DocFlavor;
import java.sql.*;
import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName UserDaoImpl
 * @date 2022/6/16 20:00
 * @Description
 */
public class UserDaoImpl implements UserDao {
    @Override
    public User queryByNp(String n, String p) throws SQLException {

        String url = "jdbc:mysql://192.168.0.210:8888/bookdb?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8";
        String username = "root";
        String password = "root";
        Connection connection = DriverManager.getConnection(url, username, password);

        String sql = "select * from book where USER_NAME = ? AND USER_PASSWORD = ?";
        PreparedStatement statement = connection.prepareStatement(sql);
        statement.setObject(1, n);
        statement.setObject(2, p);
        ResultSet resultSet = statement.executeQuery();
        User user = null;
        if (resultSet.next()) {
            user = new User();
            user.setUserName(resultSet.getString("USER_NAME"));
            user.setUserPassword(resultSet.getString("USER_PASSWORD"));
        }
        return user;
    }
}
