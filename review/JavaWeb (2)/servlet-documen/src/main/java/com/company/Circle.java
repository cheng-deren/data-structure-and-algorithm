package com.company;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Worke
 * @date 2023/1/6 10:48
 * @Description
 */
public class Circle implements ShapeInterface {

    @Override
    public double getArea(double a) {
        return PI * a * a;
    }
}
