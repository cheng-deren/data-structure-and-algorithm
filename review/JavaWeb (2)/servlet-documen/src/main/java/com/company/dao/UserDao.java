package com.company.dao;

import com.company.pojo.User;

import java.sql.SQLException;

/**
 * @author cdr
 * @version 1.0
 * @ClassName UserDao
 * @date 2022/6/16 19:59
 * @Description
 */
public interface UserDao {

    /**
     * 查询用户
     * @param n
     * @param p
     * @return
     */
    public User queryByNp(String n, String p) throws SQLException;
}
