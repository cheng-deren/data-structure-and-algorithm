package com.company.utils;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 * @author cdr
 * @version 1.0
 * @ClassName JdbcUtils
 * @date 2022/6/16 19:36
 * @Description
 */
public class JdbcUtils {


    public static Connection getConnection() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");

            //公司
            String url = "jdbc:mysql://192.168.80.216:3306/sys?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8";
            String username = "root";
            String password = "hxd@2021";



            //个人
//            String url = "jdbc:mysql://localhost:3306/test?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8";
//            String username = "root";
//            String password = "root";
            connection = DriverManager.getConnection(url, username, password);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return connection;

    }


}
