package com.company.servlet;

import com.company.dao.UserDaoImpl;
import com.company.pojo.User;
import com.company.service.UserServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName UserServlet
 * @date 2022/6/16 20:06
 * @Description
 */
public class UserServlet extends HttpServlet {

    private UserServiceImpl userService = new UserServiceImpl();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        resp.setHeader("Content-Type", "text/html;charset=UTF-8");
        String userName = req.getParameter("userName");
        String userPassword = req.getParameter("password");
        if (Objects.equals(userName, "test") && Objects.equals(userPassword, "888")) {
            req.getSession().setAttribute("user", "userName:" + userName + "userPassword:" + userPassword);
            req.getRequestDispatcher("/login/loginSucces.jsp").forward(req, resp);
            resp.getWriter().write("登录成功");
        } else {
            resp.getWriter().write("用户名或密码错误");
            req.getRequestDispatcher("/login/loginError.html").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req, resp);
    }
}
