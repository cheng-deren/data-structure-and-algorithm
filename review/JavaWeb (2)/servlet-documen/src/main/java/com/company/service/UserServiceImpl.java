package com.company.service;

import com.company.dao.UserDao;
import com.company.dao.UserDaoImpl;
import com.company.pojo.User;

import java.sql.SQLException;

/**
 * @author cdr
 * @version 1.0
 * @ClassName UserService
 * @date 2022/6/16 20:04
 * @Description
 */
public class UserServiceImpl implements UserService {

    private UserDaoImpl userDao = new UserDaoImpl();

    @Override
    public User queryByNp(String n, String p) throws SQLException {
        return userDao.queryByNp(n, p);
    }
}
