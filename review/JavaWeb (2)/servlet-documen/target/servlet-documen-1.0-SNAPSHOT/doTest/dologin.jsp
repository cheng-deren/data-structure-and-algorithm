<%@ page import="com.mysql.jdbc.StringUtils" %>
<%@ page import="java.util.Objects" %><%--
  Created by IntelliJ IDEA.
  User: 70439
  Date: 2022/6/16
  Time: 20:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>校验页面</h1>
<%
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");

    String user = request.getParameter("user");
    String pwd = request.getParameter("pwd");

    String login = "/doTest/login.jsp";
    String success = "/doTest/success.jsp";

    if (StringUtils.isNullOrEmpty(user) && StringUtils.isNullOrEmpty(pwd)) {
        request.getRequestDispatcher(login).forward(request, response);
    }
    if (!StringUtils.isNullOrEmpty(user) && StringUtils.isNullOrEmpty(pwd)) {
        request.setAttribute("user", request.getParameter("user"));
        request.getRequestDispatcher(login).forward(request, response);
    }
    if (!StringUtils.isNullOrEmpty(pwd) && StringUtils.isNullOrEmpty(user)) {
        request.setAttribute("pwd", request.getParameter("pwd"));
        request.getRequestDispatcher(login).forward(request, response);
    }
    if (!StringUtils.isNullOrEmpty(user) && !StringUtils.isNullOrEmpty(pwd)) {
        request.getRequestDispatcher(success).forward(request, response);
    }


%>

</body>
</html>
