package com.company;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Properties;

/**
 * @author cdr
 * @version 1.0
 * @ClassName TestServlet
 * @date 2022/5/29 20:18
 * @Description
 */
@WebServlet(name = "MyStreamServlet",value = "/myStream")
public class MyStreamServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String filePath = "/WEB-INF/classes/myStream.properties";
        InputStream inputStream = this.getServletContext().getResourceAsStream(filePath);
        Properties properties = new Properties();
        properties.load(inputStream);

        String key = properties.getProperty("key");
        String value = properties.getProperty("value");

        resp.setContentType("text/html");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter writer = resp.getWriter();
        writer.print(key + value);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
