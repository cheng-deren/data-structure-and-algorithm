package com.company;

import javax.servlet.ServletException;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @author cdr
 * @version 1.0
 * @ClassName HelloServlet
 * @date 2022/5/29 14:44
 * @Description TODO @WebServlet 注解版 - 注册 Servlet 所有方式都需要加 /（根）路径
 * 1.不指定servlet的 name，默认名是类名，但是首字母小写
 * 2.如果只有一个url模式，可以不写 value
 * 3.如果只需要指定url模式可以指定多个，如果有多个，则 ("/error","/error1","/error2")
 * 4.urlPatterns 和 value 属性功能等效，都用于指定 servlet的 url模式
 * TODO servlet-api版本 3.0以上才开始支持注解注册Servlet！！！
 */

@WebServlet(name = "HelloServletAnnotation", value = "/hello",initParams = {
        @WebInitParam(name = "studentName",value = "zhangsan")
})
public class HelloServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        resp.setCharacterEncoding("UTF-8");
        PrintWriter writer = resp.getWriter();
        String studentName = this.getInitParameter("studentName");
        writer.print("姓名:" + studentName);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
