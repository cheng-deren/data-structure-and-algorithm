<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.DriverManager" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<html>
<body>
学生信息如下：
<table border="1">
    <tr>
        <td>学号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>班级</td>
    </tr>
    <%
        Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        String url = "jdbc:sqlserver://localhost:1433;databaseName=WebInfo";
        String username = "sa";
        String password = "123";
        Connection conn = DriverManager.getConnection(url, username, password);
        String sql = "SELECT * FROM students";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        ResultSet resultSet = preparedStatement.executeQuery();
        request.setAttribute("resultSet", resultSet);
        while (resultSet.next()) {
    %>
    <tr>
        <td><%=resultSet.getInt("ID")%>
        </td>
        <td><%=resultSet.getString("name")%>
        </td>
        <td><%=resultSet.getInt("age")%>
        </td>
        <td><%=resultSet.getString("class")%>
        </td>
    </tr>
</table>
<%
    }
%>


</body>


</html>
