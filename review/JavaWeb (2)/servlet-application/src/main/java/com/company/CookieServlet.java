package com.company;

import com.company.constant.MyCharacter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName CookieServlet
 * @date 2022/5/30 14:27
 * @Description
 */
@WebServlet(name = "CookieServlet", value = "/servlet-application/cookie")
public class CookieServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Cookie[] cookies = req.getCookies();
        String result = null;
        for (int i = 0; i < cookies.length; i++) {
            Cookie cookie = cookies[i];
            if (Objects.equals(cookie.getName(), "key")) {
                result = cookie.getValue();
                break;
            }
        }

        if (result == null) {
            Cookie key = new Cookie("key", String.valueOf(System.currentTimeMillis()));
            key.setMaxAge(60);
            resp.addCookie(key);
            result = "Cookie初始化...";
        }

        PrintWriter writer = resp.getWriter();
        writer.print("Cookie-----" + result);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
