package com.company.filter;

import com.company.constant.MyCharacter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebListener;
import java.io.IOException;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyCharacterFilter
 * @date 2022/5/31 15:35
 * @Description
 *
 * TODO WebFilter filterName 过滤器名称  value 拦截（请求）路径
 */
@WebListener
@WebFilter(filterName = "MyCharacterFilter",value = "/servlet-application/*")
public class MyCharacterFilter implements Filter {

    /**
     * 初始化 web服务器启动
     * @param filterConfig
     * @throws ServletException
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        servletRequest.setCharacterEncoding(MyCharacter.UTF_8);
        servletResponse.setContentType("text/html;charset=UTF-8");
        servletResponse.setCharacterEncoding(MyCharacter.UTF_8);

        //过滤器放行,没有这步过滤器将卡在这里
        filterChain.doFilter(servletRequest,servletResponse);

    }

    /**
     * 销毁 web服务器关闭
     */
    @Override
    public void destroy() {

    }
}
