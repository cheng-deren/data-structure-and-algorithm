package com.company.listener;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyListener
 * @date 2022/5/31 16:00
 * @Description 监听在线人数
 */
public class MyListener implements HttpSessionListener {

    @Override
    public void sessionCreated(HttpSessionEvent se) {
        ServletContext context = se.getSession().getServletContext();
        Integer online = (Integer) context.getAttribute("online");

        if (online == null) {
            online = 1;
        } else {
            online++;
        }
        context.setAttribute("online",online);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {

    }
}
