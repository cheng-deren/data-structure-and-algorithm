package com.company.utils;

import java.sql.*;
import java.util.Date;

/**
 * @author cdr
 * @version 1.0
 * @ClassName JdbcUtil
 * @date 2022/6/16 15:13
 * @Description
 */
public class JdbcTest {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://192.168.80.216:3306/sys?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8";
            String username = "root";
            String password = "hxd@2021";
            Connection connection = DriverManager.getConnection(url, username, password);

            //查询
            String sql1 = "SELECT * FROM sys_config";
            PreparedStatement statement1 = connection.prepareStatement(sql1);
            ResultSet resultSet = statement1.executeQuery();
            while (resultSet.next()) {
                System.out.println(resultSet.getString("variable"));
            }


            //插入
            String sql2 = "INSERT INTO sys_config (variable,`value`,set_time,set_by) VALUES (?,?,?,?)";
            PreparedStatement statement2 = connection.prepareStatement(sql2);
            statement2.setObject(1, "ces2");
            statement2.setObject(2, "ces");
            statement2.setObject(3, new Date());
            statement2.setObject(4, "ces");
            int insert = statement2.executeUpdate();
            if (insert > 0) {
                System.out.println("插入成功");
            }

            //修改
            String sql3 = "UPDATE sys_config SET variable = ?,`value` = ? WHERE variable = ? ";
            PreparedStatement statement3 = connection.prepareStatement(sql3);
            statement3.setObject(1, "UPDATE");
            statement3.setObject(2, "UPDATE");
            statement3.setObject(3, "ces");
            int update = statement3.executeUpdate();
            if (update > 0) {
                System.out.println("修改成功");
            }

            //删除
            String sql4 = "DELETE FROM sys_config WHERE variable = ?";
            PreparedStatement statement4 = connection.prepareStatement(sql4);
            statement4.setObject(1, "ces1");
            int delete = statement4.executeUpdate();
            if (delete > 0) {
                System.out.println("删除成功");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
