package com.company.constant;

/**
 * @author cdr
 * @version 1.0
 * @ClassName Character
 * @date 2022/5/30 14:30
 * @Description
 */
public interface MyCharacter {
    String UTF_8 = "UTF-8";
}
