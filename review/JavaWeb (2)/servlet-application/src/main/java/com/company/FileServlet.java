package com.company;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;

/**
 * @author cdr
 * @version 1.0
 * @ClassName FileServlet
 * @date 2022/5/30 11:03
 * @Description 下载文件
 * TODO @WebServlet注解 value属性 必须要 /（根） 不然启动报错
 */
@WebServlet(name = "FileServlet", value = "/servlet-application/file")
public class FileServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //绝对路径
        String filePath = "D:\\struggle\\review\\JavaWeb\\servlet-application\\src\\main\\resources\\风景.jpg";
        String fileName = filePath.substring(filePath.lastIndexOf("\\") + 1);
        resp.setHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, StandardCharsets.UTF_8));

        //获取下载文件的输入流
        FileInputStream inputStream = new FileInputStream(filePath);
        //创建缓冲区
        int len = 0;
        byte[] buffer = new byte[1024];
        //获取ServletOutputStream
        ServletOutputStream outputStream = resp.getOutputStream();
        //将文件输入流（inputStream） 写入到缓冲区
        while ((len = inputStream.read(buffer)) > 0) {
            outputStream.write(buffer, 0, len);
        }

        inputStream.close();
        outputStream.close();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
