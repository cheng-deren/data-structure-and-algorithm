package com.company.utils;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Objects;
import java.util.Properties;

/**
 * @author cdr
 * @version 1.0
 * @ClassName BaseDao
 * @date 2022/6/6 20:48
 * @Descriptio 基础类 （创建数据源连接采用饱汉单例）
 */
public class BaseDao {

    private static final String DRIVER;
    private static final String URL;
    private static final String USERNAME;
    private static final String PASSWORD;


    private static Connection connection;
    private static PreparedStatement statement;

    private BaseDao() {
    }

    static {
        //读取配置数据源配置
        InputStream stream = BaseDao.class.getClassLoader().getResourceAsStream("database.properties");
        Properties properties = new Properties();
        try {
            properties.load(stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        DRIVER = properties.getProperty("driver");
        URL = properties.getProperty("url");
        USERNAME = properties.getProperty("username");
        PASSWORD = properties.getProperty("password");
    }


    /**
     * 建立数据源连接
     *
     * @return
     */
    public static Connection getConnection() {
        if (Objects.isNull(connection)) {
            try {
                //加载驱动
                Class.forName(DRIVER);
                //建立连接
                connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return connection;
    }


    /**
     * 关闭数据源连接
     *
     * @return
     */
    public static void close() {

        if (Objects.nonNull(statement)) {
            try {
                statement.close();
                //制空方便GC及时回收
                statement = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

        if (Objects.nonNull(connection)) {
            try {
                connection.close();
                connection = null;
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }


    /**
     * 公共查询方法
     *
     * @return
     */
    public static ResultSet query(String sql, Object[] params) {
        Connection connection = getConnection();
        ResultSet resultSet = null;
        try {
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            resultSet = statement.executeQuery();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return resultSet;
    }


    /**
     * 公共添加或修改方法
     *
     * @param sql
     * @param params
     * @return
     */
    public static int insertOrUpdate(String sql, Object[] params) {
        Connection connection = getConnection();
        int result = 0;
        try {
            statement = connection.prepareStatement(sql);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            result = statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return result;
    }


}
