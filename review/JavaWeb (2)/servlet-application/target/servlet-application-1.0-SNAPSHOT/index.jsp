<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.util.Date" %>
<html>
<body>
<h2>Hello World!</h2>
</body>

<%--测试 监听器 统计在线人数--%>
<h3>监听器:统计在线人数为 <%= request.getSession().getAttribute("online")%></h3>



<%--
    JSP 表达式
    作用：用来将程序的返回信息处理
--%>

<%--变量表达式--%>
<%= new Date()%>


<%--JSP 脚本片段--%>
<%
    int x = 100;
    int num = 0;
    while (x > 0) {
        num += x;
        x--;
    }
    out.print("<h3>1-100的总数：" + num + "</h3>");
%>

<%--JSP 脚本片段 进阶--%>
<%
    for (int i = 1; i <= 5; i++) {
        int y = i * i;
%>
<h3>hello word <%= i%></h3>
<%
        out.print(i + y);
    }
%>


<%--
    <%!%>   JSP 定义类属性及方法
    <%%>    这种方式都是在 JSP 内置的 _jspService 方法中加代码
 --%>

<%!
    static {
        System.out.println("index.jsp初始化");
    }

    private String name;
    private Integer age;

    public void show() {
        System.out.println("show方法");
    }
%>


</html>
