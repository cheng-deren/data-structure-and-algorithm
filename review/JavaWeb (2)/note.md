web.xml 头文件

```xml

<web-app xmlns="http://xmlns.jcp.org/xml/ns/javaee"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://xmlns.jcp.org/xml/ns/javaee
http://xmlns.jcp.org/xml/ns/javaee/web-app_4_0.xsd"
         version="4.0"
         metadata-complete="false">
    <!--metadata-complete="false" 开启注解扫描-->


    <servlet-mapping>
        <servlet-name>HelloServlet.xml</servlet-name>
        <url-pattern>/hello.xml</url-pattern>
    </servlet-mapping>

    <!--2.可以自定义后缀实现请求映射，但是 *.do 前面不能任何路径
        2.2.url-pattern 指定通配符路径 低于 固有映射路径 访问优先级
    -->
    <servlet-mapping>
        <servlet-name>HelloServlet.xml</servlet-name>
        <url-pattern>*.do</url-pattern>
    </servlet-mapping>

</web-app>

```



