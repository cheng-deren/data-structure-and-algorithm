package com.company.controller;

import com.company.entity.User;
import com.company.repository.UserRepository;
import com.company.repository.UserRepositoryImpl;
import com.company.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author cdr
 * @version 1.0
 * @ClassName UserController
 * @date 2022/6/21 22:19
 * @Description
 */
@Controller
public class UserController {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepositoryImpl repository;

    @RequestMapping(value = "saveUser", method = RequestMethod.GET)
    @ResponseBody
    public String savePerson(){
        userService.saveUser();
        return "success!";
    }


    @GetMapping("/queryUserList")
    @ResponseBody
    public List<User> queryUserList(){
        return repository.queryUserList("123");
    }
}
