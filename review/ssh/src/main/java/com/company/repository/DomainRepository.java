package com.company.repository;

import java.io.Serializable;
import java.util.List;

/**
 * @author cdr
 * @version 1.0
 * @ClassName a
 * @date 2022/6/21 22:13
 * @Description
 */
public interface DomainRepository<T,PK extends Serializable>{
    T load(PK id);

    T get(PK id);

    List<T> findAll();

    void persist(T entity);

    PK save(T entity);

    void saveOrUpdate(T entity);

    void delete(PK id);

    void flush();
}
