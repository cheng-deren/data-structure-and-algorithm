package com.company.repository;

import com.company.entity.User;

import java.util.List;

/**
 * @author cdr
 * @version 1.0
 * @ClassName a
 * @date 2022/6/21 22:14
 * @Description
 */
public interface UserRepository extends DomainRepository<User,Long> {

    /**
     * 查询
     * @param key
     * @return
     */
    public List<User> queryUserList(String key);

}
