package com.company.repository;

import com.company.entity.User;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author cdr
 * @version 1.0
 * @ClassName a
 * @date 2022/6/21 22:15
 * @Description
 */
@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private SessionFactory sessionFactory;

    private Session getCurrentSession() {
        return this.sessionFactory.openSession();
    }

    @Override
    public User load(Long id) {
        return (User) getCurrentSession().load(User.class, id);
    }

    @Override
    public User get(Long id) {
        return (User) getCurrentSession().get(User.class, id);
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public void persist(User entity) {
        getCurrentSession().persist(entity);
    }

    @Override
    public Long save(User entity) {
        return (Long) getCurrentSession().save(entity);
    }

    @Override
    public void saveOrUpdate(User entity) {
        getCurrentSession().saveOrUpdate(entity);
    }

    @Override
    public void delete(Long id) {
        User person = load(id);
        getCurrentSession().delete(person);
    }

    @Override
    public void flush() {
        getCurrentSession().flush();
    }

    @Override
    public List<User> queryUserList(String key) {
        String sql = "from User where USER_PASSWORD = :key";
        Query query = getCurrentSession().createQuery(sql);
        query.setString("key",key);
        return query.list();
    }
}
