package com.company.service;


/**
 * @author cdr
 * @version 1.0
 * @ClassName UserService
 * @date 2022/6/21 22:17
 * @Description
 */
public interface UserService {
    Long saveUser();
}
