package com.company.service;

import com.company.entity.User;
import com.company.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author cdr
 * @version 1.0
 * @ClassName UserServiceImpl
 * @date 2022/6/21 22:17
 * @Description
 */

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public Long saveUser() {
        User user = new User();
        user.setUserName("root");
        user.setUserPassword("root");
        return userRepository.save(user);
    }
}
