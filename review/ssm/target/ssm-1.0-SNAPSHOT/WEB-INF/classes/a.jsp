<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.Date" %>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title></title>
</head>
<body>

<%
    String hello = "Hello,World!";
    String date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
%>
<%=hello%>,
<%=date%>
</body>
</html>
