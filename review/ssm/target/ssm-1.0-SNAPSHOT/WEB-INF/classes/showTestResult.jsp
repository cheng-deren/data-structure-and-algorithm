<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<sql:setDataSource
        var="dataSource"
        url="jdbc:mysql://localhost:3306//mydatabase"
        driver="com.mysql.jdbc.Driver"
        user="sa"
        password="123456"/>
<table border="1">
    <tr>
        <td>老师id</td>
        <td>老师名字</td>
        <td>老师性别</td>
        <td>老师电话</td>
    </tr>
    <sql:update sql="update teacher set tphone = '02431975623' where tname = '张丽'/>
    <sql:query dataSource = "${dataSource}" var= "rs">
        SELECT * from teacher;
    </sql:query>
    <c:forEach var="table" items="${rs.rows}">
        <tr>
            <td><c:out value="${tno}" /></td>
            <td><c:out value="${tname}" /></td>
            <td><c:out value="${tsex}" /></td>
            <td><c:out value="${tphone}" /></td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
