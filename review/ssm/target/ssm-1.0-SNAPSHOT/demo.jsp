<%@ page language="java" import="java.sql.Connection" contentType="text/html; charset=UTF-8"
         pageEncoding="GB18030" %>
<%@ page import="java.sql.*" %>
<!DOCTYPE html>
<html>
<head>
</head>
<body>
<h1>所有用户</h1>
<table border="1" align="center">
    <tr>
        <th>序号</th>
        <th>用户名</th>
        <th>密码</th>
    </tr>
    <%
        String driverClass = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/TestData";
        String user = "root";
        String password = "mysql";
        Connection conn;
        try {
            Class.forName(driverClass);
            conn = DriverManager.getConnection(url, user, password);
            Statement stmt = conn.createStatement();
            String sql = "select * from Users";
            ResultSet rs = stmt.executeQuery(sql);
            int i = 1;
            while (rs.next()) {
    %>
    <tr>
        <td><%=i %>
        </td>
        <td><%=rs.getString("username") %>
        </td>
        <td><%=rs.getString("password") %>
        </td>
    </tr>
    <%
                i++;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    %>
</table>
</body>
</html>
