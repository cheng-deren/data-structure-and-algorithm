1)功能模块如下：
管理端：
    1）登录功能：管理员进行登录
    2）权限管理：对所有用户进行相应模块进行授权以及取消权限
    3）会员管理：对用户进行管理，包括：封禁用户，用户信息修改，添加用户，删除用户
    4）健身项目管理：管理本企业的健身项目（包括增删改查）
    5）预约管理：对用户的预约进行查看并审核，查看所有用户的预约信息，查看所有时间段用户预约数量等
    6）日志管理：记录系统登录日志，以查看后台处理操作流水信息
用户端：
    1）注册功能：用户访问网站并注册会员账号
    2）登录功能：会员用户登录网站
    3）会员个人中心：对个人信息进行增删改查
    4）健身预约：访问网站进行健身时间预约
    5）预约查询：查询健身预约是否成功
    6）会员交流中心：可以进行发帖，评论，点赞，回复等

2-系统结构：
使用MVC三层架构进行开发：
1）com.demo.controller:接收用户请求并返回数据，完成用户请求的转发及对用户的响应，为用户提供一种交互式操作的界面，相当于MVC里的C层
2）com.demo.service: 实现业务的主要逻辑，包括对数据层的操作，对数据业务逻辑处理等，是系统架构中体现核心价值的部分。
3）com.demo.dao: 其功能主要是负责数据库的访问，实现对数据表的Select，Insert，Update，Delete的操作。
4）com.demo.entity: 实体类,属于model层里面,相当于MVC的M层
5）webapp/*.jsp:jsp页面，数据可视化页面，进行数据展示，相当于MVC里的V层
其他额外所需模块：
4）com.demo.security:对用户登录进行鉴权授权
5）com.demo.util: 对常用方法进行封装，便于系统调用


3-1）源码结构
1)java
    -- com.demo.controller
    -- com.demo.service
    -- com.demo.dao
    -- com.demo.entity
    -- com.demo.security
    -- com.demo.util
2）webapp文件夹
    -- jsp文件
    -- WEB-INF
       -- web.xml
3）resources文件夹
    -- applicationContext.xml
    -- dbconfig.properties
    -- mybatis-config.xml
    -- springmvc.xml

4-1小题
1）用户id：yhid
2) 姓名：xm
2）预约时间：yysj
3) 预约项目：yyxm


5题）
身份认证开源解决方案：
1）使用spring家族的spring security
2）使用Apache shiro
3) 使用spring的oauth2
















