package com.day12.bean;

import java.util.Date;

public class TOrder {
    private long tId;
    private String goodsId;
    private String goodsName;
    private long goodsNumber;
    private Double money;
    private Date createTime;
    public long gettId() {
        return tId;
    }
    public void settId(long tId) {
        this.tId = tId;
    }
    public String getGoodsId() {
        return goodsId;
    }
    public void setGoodsId(String goodsId) {
        this.goodsId = goodsId;
    }
    public String getGoodsName() {
        return goodsName;
    }
    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }
    public long getGoodsNumber() {
        return goodsNumber;
    }
    public void setGoodsNumber(long goodsNumber) {
        this.goodsNumber = goodsNumber;
    }
    public Double getMoney() {
        return money;
    }
    public void setMoney(Double money) {
        this.money = money;
    }
    public Date getCreateTime() {
        return createTime;
    }
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}
