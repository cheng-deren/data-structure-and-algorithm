package com.day12.controller;

import com.day12.bean.TOrder;
import com.day12.service.TOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("/order")
public class TOrderController {

    @Autowired
    private TOrderService tOrderService;

    @PostMapping("/save")
    public void saveOrder(@RequestBody TOrder tOrder, HttpServletRequest request) throws UnsupportedEncodingException {
        String username=request.getParameter("username");
        username=new String(username.getBytes("iso8859-1"),"UTF-8");

        tOrderService.saveOrder(tOrder);
    }
}
