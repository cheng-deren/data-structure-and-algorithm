package com.day12.dao;

import com.day12.bean.TOrder;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TOrderDao {

    void saveOrder(TOrder tOrder);
}
