package com.day12.service;

import com.day12.bean.TOrder;
import com.day12.dao.TOrderDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TOrderService {

    @Autowired
    private TOrderDao tOrderDao;

    public void saveOrder(TOrder tOrder) {
        tOrderDao.saveOrder(tOrder);
    }
}
