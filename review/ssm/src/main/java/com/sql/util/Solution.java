package com.sql.util;

/**
 * Definition for singly-linked list.
 * public class ListNode {
 * int val;
 * ListNode next;
 * ListNode() {}
 * ListNode(int val) { this.val = val; }
 * ListNode(int val, ListNode next) { this.val = val; this.next = next; }
 * }
 */
class ListNode {
    int val;
    ListNode next;

    ListNode() {
    }

    ListNode(int val) {
        this.val = val;
    }

    ListNode(int val, ListNode next) {
        this.val = val;
        this.next = next;
    }
}

public class Solution {
    public static void main(String[] args) {
        //[7,2,4,3]
        //[5,6,4]
        ListNode l1 = new ListNode(3, null);
        ListNode l2 = new ListNode(4, l1);
        ListNode l3 = new ListNode(2, l2);
        ListNode l4 = new ListNode(7, l3);

        ListNode l5 = new ListNode(4, null);
        ListNode l6 = new ListNode(6, l5);
        ListNode l7 = new ListNode(5, l6);


        Solution solution = new Solution();
        solution.addTwoNumbers(l4, l7);
    }

    public ListNode addTwoNumbers(ListNode l1, ListNode l2) {
        ListNode L1 = reverse(l1);
        ListNode L2 = reverse(l2);
        ListNode dummyHead = new ListNode(0);
        int next = 0;
        while (L1 != null || L2 != null) {
            int value = 0;
            if (L1 != null) {
                value += L1.val;
            }
            if (L2 != null) {
                value += L2.val;
            }
            value += next;
            int newValue = value % 10;
            if (L1 != null) {
                L1.val = newValue;
                dummyHead.next = L1;

            } else {
                L2.val = newValue;
                dummyHead.next = L2;
            }
            if (L1 != null) {
                L1 = L1.next;
            }
            if (L2 != null) {
                L2 = L2.next;
            }
            next = newValue > 10 ? 1 : 0;
        }
        return dummyHead.next;
    }

    public ListNode reverse(ListNode root) {
        if (root == null || root.next == null) {
            return root;
        }
        ListNode head = reverse(root.next);
        root.next.next = root;
        root.next = null;
        return head;
    }
}
