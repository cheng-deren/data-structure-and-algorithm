package com.sql.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class StudentDao {

    public int insert(StudentBean studentBean) throws SQLException {
        Connection connection = DBUtil.getConnection();
        String sql = "insert into student (s_id, s_name, s_number, s_class) value (?, ?, ?, ?)";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, studentBean.getsId());
        ps.setString(2, studentBean.getsName());
        ps.setString(3, studentBean.getsNumber());
        ps.setString(4, studentBean.getsClass());
        return ps.executeUpdate();
    }


    public int delete(int sId) throws SQLException {
        Connection connection = DBUtil.getConnection();
        String sql = "delete from student where s_id = ?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setInt(1, sId);
        return ps.executeUpdate();
    }
}
