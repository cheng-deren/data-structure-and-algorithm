package com.sql.util;

public class StudentBean {
    private int sId;
    private String sName;
    private String sNumber;
    private String sClass;
    public int getsId() {
        return sId;
    }
    public void setsId(int sId) {
        this.sId = sId;
    }
    public String getsName() {
        return sName;
    }
    public void setsName(String sName) {
        this.sName = sName;
    }
    public String getsNumber() {
        return sNumber;
    }
    public void setsNumber(String sNumber) {
        this.sNumber = sNumber;
    }
    public String getsClass() {
        return sClass;
    }
    public void setsClass(String sClass) {
        this.sClass = sClass;
    }
}
