package com.sql.util;

import java.sql.SQLException;

public class StudentTest {
    public static void main(String[] args) throws SQLException {
        StudentBean studentBean = new StudentBean();
        studentBean.setsId(1);
        studentBean.setsName("张三");
        studentBean.setsNumber("123");
        studentBean.setsClass("Java 100班");

        StudentDao studentDao = new StudentDao();
        studentDao.insert(studentBean);
        studentDao.delete(1);
    }
}
