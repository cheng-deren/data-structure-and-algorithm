package com.sql.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class OracleDBUtil {

    private static Connection connection;

    private static final String URL_STRING = "jdbc:oracle:thin:@192.168.80.8:1521:orcl";
    private static final String USER_STRING = "syb_test";
    private static final String PASS_STRING = "syb_test2020";


    static {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static Connection getConnection() {
        try {
            connection = DriverManager.getConnection(URL_STRING, USER_STRING, PASS_STRING);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void main(String[] args) throws Exception {
        Connection connection = getConnection();
        String sql = "select\n" +
                "       main.KNOWLEDGE_ID,\n" +
                "       main.KM_NO,\n" +
                "       main.TITLE,\n" +
                "       main.KEY_WORD,\n" +
                "       main.DESCP,\n" +
                "       to_char(main.CONTENT),\n" +
                "       main.VERSON,\n" +
                "       main.PLATE_TYPE,\n" +
                "       main.SUB_LIBRARY,\n" +
                "       main.CATEGORY,\n" +
                "       main.SUB_CATEGORY,\n" +
                "       main.TEMP_TYPE,\n" +
                "       main.DEPT,\n" +
                "       main.POST,\n" +
                "       main.COVER_URL,\n" +
                "       main.AUTHOR_TYPE,\n" +
                "       main.AUTHOR,\n" +
                "       main.DOC_STATUS,\n" +
                "       main.COLLECT,\n" +
                "       main.SHARE_COUNT,\n" +
                "       main.DOWNLOAD_COUNT,\n" +
                "       main.EFFECT_TIME,\n" +
                "       main.UBLISH_DATE,\n" +
                "       main.IS_COMMENT,\n" +
                "       main.URL,\n" +
                "       main.VIEW_DEPT,\n" +
                "       main.APPROVE_STATUS,\n" +
                "       to_char(main.EXTEND),\n" +
                "       main.STATUS,\n" +
                "       main.ORDER_NO,\n" +
                "       main.REMARK,\n" +
                "       main.CREATE_BY,\n" +
                "       main.CREATE_DATE,\n" +
                "       main.UPDATE_BY,\n" +
                "       main.UPDATE_DATE,\n" +
                "       main.IS_WATERMARK,\n" +
                "       main.TEXT_INPUT_TYPE,\n" +
                "       main.CONTENT_FILE_URL,\n" +
                "       main.DOWNLOAD_TYPE,\n" +
                "       to_char(main.DOWNLOAD_USER),\n" +
                "       to_char(main.VIEW_USER),\n" +
                "       main.PIPELINE_NUMBER,\n" +
                "       main.CLICK,\n" +
                "       main.DATUM_TYPE,\n" +
                "       main.VALID_STATUS,\n" +
                "       user1.NAME       as CREATE_BY_NAME,\n" +
                "       user2.NAME       as UPDATE_BY_NAME,\n" +
                "       user3.NAME       as AUTHOR_NAME,\n" +
                "       user4.NAME       as DOWNLOAD_USER_NAME,\n" +
                "       group1.UNIT_NAME as DEPT_NAME,\n" +
                "       category.NAME as CATEGORY_NAME\n" +
                "from TBL_KMS_KNOWLEDGE main\n" +
                "         left join TBL_HRM_PERSON user1 on main.CREATE_BY = user1.PERSON_ID\n" +
                "         left join TBL_HRM_PERSON user2 on main.UPDATE_BY = user2.PERSON_ID\n" +
                "         left join TBL_HRM_PERSON user3 on main.AUTHOR = user3.PERSON_ID\n" +
                "         left join TBL_HRM_PERSON user4 on main.DOWNLOAD_USER = user4.PERSON_ID\n" +
                "         left join TBL_HRM_UNIT_INFO group1 on group1.UNIT_ID = main.DEPT\n" +
                "        left join TBL_KMS_SORT category on main.CATEGORY = category.SORT_ID\n" +
                "where main.APPROVE_STATUS = '2' and main.STATUS = '0'";
        PreparedStatement ps = connection.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        List<String> list = new ArrayList<>();
        while (rs.next()) {
            String title = rs.getString("TITLE");
//            Clob content = rs.getClob("CONTENT");
            String content = rs.getString("CONTENT");
//            System.out.println(content.length());
//            System.out.println(content);
//            String s = clob2String(content);
//            System.out.println(s);
//
//            oracle.sql.CLOB clob = (oracle.sql.CLOB) rs
//                    .getClob("CONTENT");
//            String value = clob.getSubString(1, (int) clob.length());
//            System.out.println("CLOB字段的值：" + value);
            list.add(content);

        }
    }

    private static String clob2String(Clob clob) throws SQLException, IOException
    {
        String reString = "";
        Reader is = clob.getCharacterStream();// 得到流
        BufferedReader br = new BufferedReader(is);
        String s = br.readLine();
        StringBuffer sb = new StringBuffer();
        while (s != null)
        {
            // 执行循环将字符串全部取出付值给StringBuffer由StringBuffer转成STRING
            sb.append(s);
            s = br.readLine();
        }
        reString = sb.toString();
        return reString;
    }



}
