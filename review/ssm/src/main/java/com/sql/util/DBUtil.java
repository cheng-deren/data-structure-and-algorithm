package com.sql.util;

import java.sql.*;

public class DBUtil {

    private static Connection connection;

    private static final String URL_STRING = "jdbc:mysql://192.168.19.51:3306/old_database?useSSL=false";
    private static final String USER_STRING = "root";
    private static final String PASS_STRING = "root";


    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public static Connection getConnection() {
        try {
            connection = DriverManager.getConnection(URL_STRING, USER_STRING, PASS_STRING);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return connection;
    }

    public static void main(String[] args) throws SQLException {
        Connection connection = getConnection();
        String sql = "select * from tbl_kms_knowledge where KNOWLEDGE_ID = '10'";
        PreparedStatement ps = connection.prepareStatement(sql);
        ResultSet rs = ps.executeQuery();
        while (rs.next()) {
            String content = rs.getString("CONTENT");
            System.out.println(content.length());
        }
    }

}
