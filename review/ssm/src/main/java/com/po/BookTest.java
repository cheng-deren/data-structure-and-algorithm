package com.po;
class Book {
    private String name;
    private String author;
    private double price;
    private int amount;

    public Book(String name, String author, double price, int amount) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public String getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double totalPrice() {
        return price * amount;
    }
}
public class BookTest {
    public static void main(String[] args) {
        Book book = new Book("Java","张三", 30, 2000);
        System.out.println("图书的总价:" + book.totalPrice());
    }
}
