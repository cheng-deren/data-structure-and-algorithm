package com.po;

import org.springframework.core.convert.converter.Converter;

public class GoodsConverter implements Converter<String, Goods> {
    @Override
    public Goods convert(String str) {
        Goods goods = new Goods();
        if (str != null) {
            String[] array = str.split(",");
            if (array != null && array.length > 0) {
                for (int i = 0; i < array.length; i++) {
                    if (i == 0) {
                        goods.setGoodsname(array[0]);
                    } else if (i == 1) {
                        goods.setGoodsprice(array[1]);
                    } else if (i == 2) {
                        goods.setGoodsnumber(array[2]);
                    } else {
                        goods.setMemorySize(array[3]);
                    }
                }
            }
        }
        return goods;
    }

    public static void main(String[] args) {
        int count = 0;
        int i = 1;
        for (; i < 30; i++) {
            if (i % 5 != 0) {
                continue;
            }
            System.out.println(i);
            count++;
        }
        System.out.println("符合条件的数据共有" + count + "个");
    }
}
