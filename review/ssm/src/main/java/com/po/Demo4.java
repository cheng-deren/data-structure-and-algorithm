package com.po;

import java.util.ArrayList;
import java.util.Iterator;

public class Demo4 {
    public static void main(String[] args) {
        int a1 = 20;
        int a2 = 30;
        int a3 = 40;
        ArrayList<Integer> arrayList = new ArrayList();
        arrayList.add(a1);
        arrayList.add(a2);
        arrayList.add(a3);
        for (int a : arrayList) {
            System.out.println(a);
        }

        Iterator<Integer> iterator = arrayList.iterator();
        while (iterator.hasNext()) {
            Integer value = iterator.next();
            if (value == a1) {
                iterator.remove();
            }
        }
    }
}
