package com.po;

import java.awt.*;

public class Demo3 {
    private Frame frame = new Frame("菜单示例");
    private MenuBar menuBar = new MenuBar();
    private Menu fileMenu = new Menu("文件");
    private Menu helpMenu = new Menu("帮助");
    private MenuItem newItem = new MenuItem("打开");
    private MenuItem saveItem = new MenuItem("保存");
    private MenuItem exitItem = new MenuItem("关闭");
    private TextArea ta = new TextArea(6, 40);


    public void init() {
        fileMenu.add(newItem);
        fileMenu.add(saveItem);
        fileMenu.add(exitItem);
        frame.add(ta);
        menuBar.add(fileMenu);
        menuBar.add(helpMenu);
        frame.setMenuBar(menuBar);
        frame.pack();
        frame.setVisible(true);
    }
    public static void main(String[] args) {
        new Demo3().init();
    }
}


