package com.demo.entity;

import java.util.ArrayList;
import java.util.List;

class Judge {
    public String no;
    public String name;
    public double score1;
    public double score2;
    public double score3;
    public double score4;

    public Judge(String no, String name, double score1, double score2, double score3, double score4) {
        this.no = no;
        this.name = name;
        this.score1 = score1;
        this.score2 = score2;
        this.score3 = score3;
        this.score4 = score4;
    }
}

public class Client {

    public List<Judge> list;

    public Client() {
        Judge judge1 = new Judge("0001", "刘爽", 9.5, 9.2, 9.6, 9);
        Judge judge2 = new Judge("0002", "王全胜", 10, 9.8, 9.7, 9.9);
        Judge judge3 = new Judge("0003", "李维", 8.6, 9.1, 9, 8.9);
        Judge judge4 = new Judge("0004", "吴宇轩", 9.2, 8.9, 9.1, 9);
        this.list = new ArrayList<>();
        list.add(judge1);
        list.add(judge2);
        list.add(judge3);
        list.add(judge4);
    }


    public double getFinalScore(Judge judge) {
        double avg = (judge.score1 + judge.score2 + judge.score3 + judge.score4) / 4;
        return avg;
    }

    public void printData() {
        System.out.print("选手编号" + "\t");
        System.out.print("姓名" + "\t");
        System.out.print("最终得分" + "\t");
        System.out.println();
        for (Judge judge : list) {
            System.out.print(judge.no + "\t");
            System.out.print(judge.name + "\t");
            System.out.print(getFinalScore(judge) + "\t");
            System.out.println();
        }
    }

    public static void main(String[] args) {
        Client client = new Client();
        client.printData();
    }
}
