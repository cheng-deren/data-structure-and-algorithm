package com.demo.entity;

import java.util.Scanner;

public class Demo5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int[] arr = new int[n];
        for (int i = 0; i < n; i++) {
            arr[i] = scanner.nextInt();
        }
        int min = 0;
        for (int i = 1; i < n; i++) {
            min = arr[i] < arr[min] ? i : min;
        }
        int temp = arr[min];
        arr[min] = arr[arr.length - 1];
        arr[arr.length - 1] = temp;
        for (int i = 0; i < n; i++) {
            System.out.print(arr[i] + ",");
        }
    }
}
class Parent {
    public void mou(int a, int b) {

    }
}
class Child extends Parent {


}