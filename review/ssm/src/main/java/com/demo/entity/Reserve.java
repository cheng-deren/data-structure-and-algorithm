package com.demo.entity;

import java.util.Date;

/**
 * 1）用户id：yhid
 * 2) 用户姓名：xm
 * 2）预约时间：yysj
 * 3) 预约项目id：yyxmid
 * 3) 预约项目名称：yyxmmc
 */
public class Reserve {
    private String reserveId;
    private String yhid;
    private String xm;
    private Date yysj;
    private String yyxmid;
    private String yyxmmc;
    private String status;
    public String getReserveId() {
        return reserveId;
    }
    public void setReserveId(String reserveId) {
        this.reserveId = reserveId;
    }
    public String getYhid() {
        return yhid;
    }
    public void setYhid(String yhid) {
        this.yhid = yhid;
    }
    public String getXm() {
        return xm;
    }
    public void setXm(String xm) {
        this.xm = xm;
    }
    public Date getYysj() {
        return yysj;
    }
    public void setYysj(Date yysj) {
        this.yysj = yysj;
    }
    public String getYyxmid() {
        return yyxmid;
    }
    public void setYyxmid(String yyxmid) {
        this.yyxmid = yyxmid;
    }
    public String getYyxmmc() {
        return yyxmmc;
    }
    public void setYyxmmc(String yyxmmc) {
        this.yyxmmc = yyxmmc;
    }
    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
}
