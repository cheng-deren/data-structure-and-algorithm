package com.demo.entity;

/**
32题）
1)a - m
2)isPrime(n)
3)break
4)int x
5)return true
 */
public class Demo111 {
    public static void main(String[] args) {
        int a, n, m;
        for (a = 6; a < 100; a = a + 2) {
            for (m = 3; m <= a / 2; m++) {
                if (isPrime(m)) {
                    n = a - m;
                    if (isPrime(n)) {
                        System.out.println(a + "=" + m + "+" + n);
                        break;
                    }
                }
            }
        }

    }
    static boolean isPrime(int x) {
        if (x == 1) {
            return false;
        }
        for (int div = 2;div < Math.sqrt(x); div++) {
            if (x % div == 0) {
                return false;
            }
        }
        return true;
    }
}
