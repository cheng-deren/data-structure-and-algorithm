package com.demo.controller;

import java.util.*;

/**
 填空
 1 抽象
 2 final
 3 Cloneable
 4 extends implements  extends
 5 this  super
 6 default protected
 */
public class StudentInfo  {
    public String number;
    public String name;
    public int age;
    public String sex;

    public StudentInfo(String number, String name, int age, String sex) {
        this.number = number;
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int length = Integer.parseInt(scanner.nextLine());
        Map<String, StudentInfo> map = new HashMap<>(length);
        for (int i = 0; i < length; i++) {
            String str = scanner.nextLine();
            String[] array = str.split(" ");
            StudentInfo studentInfo = new StudentInfo(array[0], array[1], Integer.parseInt(array[2]), array[3]);
            map.put(array[0], studentInfo);

        }
        StudentInfo[] studentInfos = new StudentInfo[map.size()];
        int count = 0;
        for (Map.Entry<String, StudentInfo> entry : map.entrySet()) {
            StudentInfo value = entry.getValue();
            studentInfos[count] = value;
            count++;
        }
        Arrays.sort(studentInfos, Comparator.comparingInt(o -> Integer.parseInt(o.number)));
        System.out.println(studentInfos.length);
        for (int i = 0; i < studentInfos.length; i++) {
            System.out.print(studentInfos[i].number + " ");
            System.out.print(studentInfos[i].name + " ");
            System.out.print(studentInfos[i].age + " ");
            System.out.println(studentInfos[i].sex + " ");
        }
    }
}
