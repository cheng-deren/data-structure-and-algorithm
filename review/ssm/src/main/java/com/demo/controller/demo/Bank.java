package com.demo.controller.demo;

public class Bank {

    public static int balance;

    public static synchronized void withdrawMoney(int money) {
        if (balance < money) {
            System.out.println("余额不足,取款失败!");
        } else {
            balance -= money;
            System.out.println("取款成功,取款金额为:" + money);
        }
    }

    public static synchronized void saveMoney(int money) {
        balance += money;
        System.out.println("存款成功,账户余额为:" + balance);
    }

}
