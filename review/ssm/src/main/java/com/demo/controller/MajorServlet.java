package com.demo.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet("/majorServlet")
public class MajorServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String scode = req.getParameter("scode");
        Connection connection = null;
        PreparedStatement ps = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test", "root", "root");
            String sql = "select * from major where scode = ?";
            ps = connection.prepareStatement(sql);
            ps.setString(1, scode);
            ResultSet rs = ps.executeQuery();
            StringBuilder str = new StringBuilder();
            while (rs.next()) {
                str
                        .append(rs.getString("mcode"))
                        .append("-")
                        .append(rs.getString("name"))
                        .append("-")
                        .append(rs.getString("scode"))
                        .append(",");
            }
            ps.close();
            connection.close();
            resp.getWriter().write(str.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
