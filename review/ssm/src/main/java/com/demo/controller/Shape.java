package com.demo.controller;

public interface Shape {
    double getArea();
    double getPerim();
}
