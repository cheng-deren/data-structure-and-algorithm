package com.demo.controller.demo;

public class Child implements Runnable{

    public int saveMoney;

    public Child(int saveMoney) {
        this.saveMoney = saveMoney;
    }

    @Override
    public void run() {
        Bank.saveMoney(saveMoney);
    }
}
