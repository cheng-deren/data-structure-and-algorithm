package com.demo.controller.demo;

public class Parent implements Runnable{

    public int withdrawMoney;

    public Parent(int withdrawMoney) {
        this.withdrawMoney = withdrawMoney;
    }

    @Override
    public void run() {
        Bank.withdrawMoney(withdrawMoney);
    }
}
