package com.demo.controller;

public class IntReverse {
    public static void main(String[] s) {
        IntReverse ir = new IntReverse();
        int[] a = {1234567899, 123, -123, 120, 0, 1234567891};
        for (int i = 0; i < a.length; ++i) {
            int ai = a[i];
            int rn = ir.reverse(ai);
            System.out.printf("%d翻转后：%d\n", ai, rn);
        }
    }

    public int reverse(int x) {
        long r = 0; //使用long类型存储转换结果，对于溢出的问题，只需要在计算过程将r强转为int后再与r比较，观察是否相等即可判定
        while (x != 0) {
            if (r < Integer.MIN_VALUE / 10 || r > Integer.MAX_VALUE / 10) {
                return 0;
            }
            int digit = x % 10;
            x /= 10;
            r = r * 10 + digit;
        }
        return (int) r;
    }
}
