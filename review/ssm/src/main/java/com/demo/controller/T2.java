package com.demo.controller;

/**
 3-2题：
 1)  String[] array = new String[]{"open","door","the","open","name"};
 2) for (int i = 0; i < array.length; i++)
 3) if ("open".equals(array[i]))
 4) count++;
 5) for (int i = array.length - 1; i >= 0; i--)
 6) System.out.println(array[i]);
 */
public class T2 {
    public static void main(String[] args) {
        String[] array = new String[]{"open","door","the","open","name"};
        int count = 0;
        for (int i = 0; i < array.length; i++) {
            if ("open".equals(array[i]))
                count++;

        }
        for (int i = array.length - 1; i >= 0; i--) {
            System.out.println(array[i]);
        }
    }
}
