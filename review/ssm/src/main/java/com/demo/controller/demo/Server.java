package com.demo.controller.demo;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws Exception {
        ServerSocket serverSocket = new ServerSocket(9999);
        Socket socket = serverSocket.accept();
        InputStream is = socket.getInputStream();
        OutputStream os = socket.getOutputStream();
        byte[] b = new byte[1024];
        int len = 0;
        String message = null;
        while ((len = is.read(b)) != -1) {
            message = new String(b);
        }
        System.out.println(("欢迎" + message.split("---")[1]));
        os.write(("欢迎" + message.split("---")[1]).getBytes());
        os.flush();
    }
}
