package com.demo.controller;

import java.util.Scanner;

class Animal {
    public String name;
    public String color;

    public void introduce() {
    }
}

class Dog extends Animal {
    public Dog(String name, String color, int iq) {
        this.name = name;
        this.color = color;
        this.iq = iq;
    }

    public int iq;

    public void catchFrisbee() {
        System.out.println("catch frisbee");
    }

    public void introduce() {
        System.out.println("My name is " + name + ", " + "my color is " + color + ", " + "my IQ is " + iq);
    }

}

class Cat extends Animal {

    public String eyeColor;

    public Cat(String name, String color, String eyeColor) {
        this.name = name;
        this.color = color;
        this.eyeColor = eyeColor;
    }

    public void catchMouse() {
        System.out.println("catch mouse");
    }

    public void introduce() {
        System.out.println("My name is " + name + ", " + "my color is " + color + ", " + "my eyeColor is " + eyeColor);
    }
}

class TestAnimal {
    public static void introduce(Animal animal) {
        animal.introduce();
    }

    public static void action(Animal animal) {
        if (animal instanceof Dog) {
            Dog dog = (Dog) animal;
            dog.catchFrisbee();
        } else if (animal instanceof Cat) {
            Cat cat = (Cat) animal;
            cat.catchMouse();
        }
    }

}

public class Main {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        int i = s.nextInt();
        Animal a = null;
        if (i == 1) {
            a = new Cat(s.next(), s.next(), s.next());
        }else if (i == 2) {
            a = new Dog(s.next(), s.next(), s.nextInt());
        }
        TestAnimal.introduce(a);
        TestAnimal.action(a);
    }
}
