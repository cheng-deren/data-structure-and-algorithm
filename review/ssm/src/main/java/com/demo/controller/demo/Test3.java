package com.demo.controller.demo;

public class Test3 {
    public static void main(String[] args) {
        Child child1 = new Child(300);
        Child child2 = new Child(400);
        Child child3 = new Child(500);
        Parent parent = new Parent(2000);
        Thread thread1 = new Thread(child1);
        Thread thread2 = new Thread(child2);
        Thread thread3 = new Thread(child3);
        Thread parentThread = new Thread(parent);
        thread1.start();
        thread2.start();
        thread3.start();
        parentThread.start();
    }
}
