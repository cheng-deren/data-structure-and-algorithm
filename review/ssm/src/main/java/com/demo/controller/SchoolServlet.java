package com.demo.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;

@WebServlet("/schoolServlet")
public class SchoolServlet  extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Connection connection = null;
        PreparedStatement ps = null;

        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/test", "root", "root");
            String sql = "select * from school";
            ps = connection.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            StringBuilder str = new StringBuilder();
            while (rs.next()) {
                str.append(rs.getString("scode")).append("-").append(rs.getString("name")).append(",");
            }
            ps.close();
            connection.close();
            resp.getWriter().write(str.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
