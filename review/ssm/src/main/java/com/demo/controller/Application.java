package com.demo.controller;

public class Application {
    public static String name = "My name is Networkcrazy";

    public static void main(String[] args) {
        System.out.println(name.length());
        System.out.println(name.charAt(0));
        System.out.println(name.charAt(name.length() - 1));
        System.out.println(name.substring(0,2));
        System.out.println(name.substring(11));
        System.out.println(name.indexOf("crazy"));
        name = name.replace("y","e");
        System.out.println(name);
        System.out.println(name.toUpperCase());
    }
}
