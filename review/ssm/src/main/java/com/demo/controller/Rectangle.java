package com.demo.controller;

public class Rectangle implements Shape{
    double length;
    double width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public double getArea() {
        return length * width;
    }

    public double getPerim() {
        return (length + width) * 2;
    }
}
