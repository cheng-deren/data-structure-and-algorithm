package com.demo.controller.demo;

public class Teacher extends Person {
    public int salary;

    public Teacher() {
    }

    public Teacher(String name, int age, int salary) {
        super(name, age);
        this.salary = salary;
    }

    public void print() {
        System.out.println("name: " + name + ", age: " + age + ", salary: " + salary);
    }
}
