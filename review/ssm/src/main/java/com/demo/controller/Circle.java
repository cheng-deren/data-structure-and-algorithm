package com.demo.controller;

public class Circle  implements Shape{
    double radius;
    public Circle(double radius) {
        this.radius = radius;
    }
    public double getArea() {
        return Math.PI * radius * radius;
    }
    public double getPerim() {
        return Math.PI * 2 * radius;
    }
}
