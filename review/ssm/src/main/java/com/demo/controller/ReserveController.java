package com.demo.controller;


import com.demo.entity.Reserve;
import com.demo.service.ReserveService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import javax.annotation.Resource;

@Controller
public class ReserveController {

    @Resource
    private ReserveService reserveService;

    @GetMapping("insert")
    public String insertReserve(Reserve reserve) {
        int result = reserveService.insertReserve(reserve);
        if (result < 1) {
            return "success.jsp";
        }else {
            return "fail.jsp";
        }
    }
}
