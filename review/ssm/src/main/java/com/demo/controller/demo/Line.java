package com.demo.controller.demo;

public class Line  implements Shape{

    private XLine xLine;

    public Line(XLine xLine) {
        this.xLine = xLine;
    }

    public void draw() {
        xLine.drawLine();
    }
}
