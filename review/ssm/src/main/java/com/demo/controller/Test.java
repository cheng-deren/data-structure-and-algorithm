package com.demo.controller;

public class Test {
    public static void main(String[] args) {
        Shape circle = new Circle(10);
        System.out.println("圆的面积：" + circle.getArea());
        System.out.println("圆的周长：" + circle.getPerim());
        Shape rectangle = new Rectangle(10,20);
        System.out.println("长方形的面积：" + rectangle.getArea());
        System.out.println("长方形的周长：" + rectangle.getPerim());
    }
}
