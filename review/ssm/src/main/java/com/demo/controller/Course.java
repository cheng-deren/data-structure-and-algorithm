package com.demo.controller;


public class Course {
   private String cid;
   private String name;
   private int credits;
   private String year;
   private static int count1;
   private static int count2;
   private static int count3;

    public Course(int count1, int count2, int count3) {
        Course.count1 = count1;
        Course.count2 = count2;
        Course.count3 = count3;
    }

    public static void printCount() {
        System.out.println("第一学年总学分: " +  count1);
        System.out.println("第二学年总学分: " +  count2);
        System.out.println("第三学年总学分: " +  count3);
    }

    public static void main(String[] args) {
        Course course1 = new Course(100,200,300);
        course1.printCount();
        Course course2 = new Course(200,300,400);
        course2.printCount();
    }

}
