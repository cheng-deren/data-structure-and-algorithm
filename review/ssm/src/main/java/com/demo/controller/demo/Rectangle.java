package com.demo.controller.demo;

public class Rectangle implements Perimeter{

    public double height;
    public double width;

    public Rectangle(double height, double width) {
        this.height = height;
        this.width = width;
    }

    @Override
    public double w_perimeter() {
        return 2 * width * height;
    }
}
