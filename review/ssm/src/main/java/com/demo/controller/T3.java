package com.demo.controller;

import javax.swing.*;
import java.awt.*;

/**
 1) JPanel p = new JPanel();
 2) f.add(p);
 3) p.setBackground(Color.RED);
 4) p.setLayout(new FlowLayout());
 5)
 6) JButton b = new JButton();
 7) p.add(b);
 8)  JComboBox c = new JComboBox();
 9) c.addItem("北京");
 10) c.addItem("上海");
 11) c.addItem("天津");
 12)  f.add(c);
 13) f.setSize(600,400);
 14) f.setVisible(true);
 */
public class T3 {
    public static void main(String[] args) {
        Frame f = new Frame("my Frame");
        JPanel p = new JPanel();
        f.add(p);
        p.setBackground(Color.RED);
        p.setLayout(new FlowLayout());
        JButton b = new JButton("b");
        p.add(b);
        JComboBox c = new JComboBox();
        c.addItem("北京");
        c.addItem("上海");
        c.addItem("天津");
        f.add(c);
        f.setSize(600,400);
        f.setVisible(true);
    }
}
