package com.demo.controller;

public class Cuboid {
    public double width;
    public double height;
    public double length;

    public void setBox(double width, double height, double length) {
        this.width = width;
        this.height = height;
        this.length = length;
    }
    public double volume() {
        return width * height * length;
    }
    public double area() {
        return 2 * (length * width + length * height + width * height);
    }

    public static void main(String[] args) {
        Cuboid b = new Cuboid();
        b.setBox(100,200,300);
        System.out.println(b.volume());
        System.out.println(b.area());
    }
}
