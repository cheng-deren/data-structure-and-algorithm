package com.demo.controller;

/**
3-1)
 1）导入处理由 AWT 组件所激发的各类事件的接口和类
 2）创建num1文本框组件对象
 3）设置num2可以编辑
 4）添加一个窗口监听 MywindowClosing
 5) 将面板的布局设置为流式布局管理器对象
 6)设置窗口背景颜色为红色
 7)创建一个实现ActionListener的Listener事件监听类
 8）将num1输入的文本转换成Double类型
 9）将n1进行计算后的数值强转为double
 10)定义MywindowClosing窗口事件监听实例类
 */


class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.sortArray(new int[] {5,1,1,2,0,0});
    }
    public int[] sortArray(int[] nums) {
        int length = nums.length;
        // for (int i = 0; i < length - 1; i++) {
        //     int minIndex = i;
        //     for (int j = i + 1; j < length; j++) {
        //         if (nums[j] < nums[minIndex]) {
        //             minIndex = j;
        //         }
        //     }
        //     swap(nums, i, minIndex);
        // }
        // return nums;
        for (int i = 0; i < length - 1; i++) {
            for (int j = 1; j < length; j++) {
                if (nums[j] < nums[j - 1]) {
                    swap(nums, j - 1, j);
                }
            }
        }
        return nums;
    }
    public void swap(int[] nums, int i, int j) {
        int temp = nums[i];
        nums[i] = nums[j];
        nums[j] = temp;
    }
}
