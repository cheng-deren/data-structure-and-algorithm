package com.demo.test;

import java.util.*;

public class Demo2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] str = line.split(" ");
            System.out.println(josephRingMethod(Integer.parseInt(str[0]), Integer.parseInt(str[1])));
        }
    }
    public static int josephRingMethod(int n, int m) {
        LinkedList<Integer> linkedList = new LinkedList();
        for (int i = 1; i <= n; i++) {
            linkedList.add(i);
        }
        int index = m - 1;
        while (linkedList.size() != 0) {
            index %= linkedList.size();
            linkedList.remove(index);
            if (linkedList.size() == 1) {
                return linkedList.get(0);
            }
            index += m - 1;
        }
        return -1;
    }
}
