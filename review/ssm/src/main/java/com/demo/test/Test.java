package com.demo.test;

/**
 * 19)
 * 1)InputMismatchException
 * 2)continue
 */
class Chart {
    public void display() {
    }
}

class PieChart extends Chart {
    public void display() {
    }
}

class BarChart extends Chart {
    public void display() {
    }
}

public class Test {
    public static void pri(String s) {
        System.out.println(s + 6);
    }

    public static void pri(int s) {
        System.out.println(s * 5);
    }

    public static void pri(int[] a) {
        int sum = 0;
        for (int i = 0; i < a.length; i++) {
            sum += a[i];
        }
        System.out.println(sum);
    }

    public static void main(String[] args) {
        int[] a = {2, 4, 6, 8, 10};
        pri(a);
        byte b = 123;
        double aa = 1;
        char aaa = 'a' + 1;
        float aaaa = 1.0f;
    }
}
