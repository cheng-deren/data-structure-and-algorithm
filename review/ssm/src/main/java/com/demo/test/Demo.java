package com.demo.test;

import java.util.Scanner;

/**
 填空4 Throwable  Exception  throw
 4-1题
 1）package
 2）int [][]
 3) static
 4) int i = 0; i < arr.length; i++
 5) int j = 0; j < arr[i].length; j++
 7) sum += arr[i][j];
 */
public abstract class Demo   {
    int x;
    public static void main(String[] args) {
        int r = (int) (Math.random() * 100 + 1);
        System.out.println(r);
        System.out.println("请输入猜测:");
        Scanner input = new Scanner(System.in);
        int guess = input.nextInt();
        int count = 0;
        count++;
        while (guess != r) {
            System.out.println("请输入猜测:");
            guess = input.nextInt();
            count++;
        }
        System.out.println(count);


    }
}
