package com.demo.test;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TestResultDao extends BaseDao{

    public List<TestResult> getResultByCondition(String name, String start, String end) throws SQLException {
        Connection connection = getConnection();
        String sql = "select * from testresult where name like ? and testDate >= ? and testDate <= ?";
        PreparedStatement ps = connection.prepareStatement(sql);
        ps.setString(1, name + "%");
        ps.setString(2, start);
        ps.setString(3, end);
        ResultSet rs = ps.executeQuery();
        List<TestResult> list = new ArrayList<>();
        while (rs.next()) {
            TestResult testResult = new TestResult();
            testResult.setId(rs.getString("id"));
            testResult.setName(rs.getString("name"));
            testResult.setTestResult(rs.getString("testResult"));
            testResult.setTestDate(rs.getString("testDate"));
            list.add(testResult);
        }
        return list;
    }
}
