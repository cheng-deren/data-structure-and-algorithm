package com.demo.test;

public interface ShapeFeatrue {
    double getArea();
    double getPerimeter();
}
