package com.demo.test;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

/**
 * 4-2：
 * 1）sum = 0
 * 2） if (n % k == 0)
 * 3） sum == n
 */
@WebServlet("/queryTestResultServlet")
public class QueryTestResultServlet extends HttpServlet {

    public static void main(String[] args) {
        int r = (int) (Math.random() * 100 + 1);
        System.out.println(r);
        System.out.println("请输入猜测:");
        Scanner input = new Scanner(System.in);
        int guess = input.nextInt();
        int count = 0;
        count++;
        while (guess != r) {
            System.out.println("请输入猜测:");
            guess = input.nextInt();
            count++;
        }
        System.out.println(count);


    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String start = req.getParameter("start");
        String end = req.getParameter("end");
        TestResultDao testResultDao = new TestResultDao();
        try {
            List<TestResult> result = testResultDao.getResultByCondition(name, start, end);
            req.setAttribute("list", result);
            req.getRequestDispatcher("showTestResult").forward(req, resp);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }
}
