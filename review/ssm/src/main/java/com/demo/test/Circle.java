package com.demo.test;

public class Circle implements ShapeFeatrue {

    public double radius;

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    @Override
    public double getArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double getPerimeter() {
        return radius * 2 * Math.PI;
    }

    public static void main(String[] args) {
        Circle circle = new Circle();
        circle.radius = 1;
        System.out.println("圆形的面积："+circle.getArea());
        System.out.println("圆形的周长："+circle.getPerimeter());
    }
}
