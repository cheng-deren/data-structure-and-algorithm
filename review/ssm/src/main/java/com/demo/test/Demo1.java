package com.demo.test;

import java.util.Scanner;

public class Demo1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNext()) {
            int num = scanner.nextInt();
            for (int i = 1; i <= num; i++) {
                for (int j = i; j <= num; j++) {
                    System.out.print(j + "  ");
                }
                System.out.println();
            }
            System.out.println();
        }
    }
}
