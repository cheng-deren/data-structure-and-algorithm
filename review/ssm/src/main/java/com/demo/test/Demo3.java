package com.demo.test;

import java.util.Scanner;

public class Demo3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Fractions fractions = new Fractions();
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] str = line.split(" ");
            String play = str[0];
            int fson1 = Integer.parseInt(str[1]);
            int fmom1 = Integer.parseInt(str[2]);
            int fson2 = Integer.parseInt(str[3]);
            int fmom2 = Integer.parseInt(str[4]);
            switch (play) {
                case "a":
                    fractions.fAdd(fson1, fmom1, fson2, fmom2);
                    break;
                case "s":
                    fractions.fSub(fson1, fmom1, fson2, fmom2);
                    break;
                case "m":
                    fractions.fMul(fson1, fmom1, fson2, fmom2);
                    break;
                case "d":
                    fractions.fDiv(fson1, fmom1, fson2, fmom2);
                    break;
                default:
                    ;
            }
        }
    }
}

class Fractions {

    public int gcd(int a, int b) {
        if (a < b) {
            int c = a;
            a = b;
            b = c;
        }
        int r = a % b;
        while (r != 0) {
            a = b;
            b = r;
            r = a % b;
        }
        return b;

    }

    public void fAdd(int fson1, int fmom1, int fson2, int fmom2) {
        int fm = fmom1 * fmom2;
        fson1 = fson1 * fmom2;
        fson2 = fson2 * fmom1;
        int fs = fson1 + fson2;
        if (fs == 0) {
            System.out.println("0");
        } else {
            int g = gcd(fs, fm);
            fs = fs / g;
            fm = fm / g;
            System.out.println(fs + "/" + fm);
        }

    }

    public void fSub(int fson1, int fmom1, int fson2, int fmom2) {
        int fm = fmom1 * fmom2;
        fson1 = fson1 * fmom2;
        fson2 = fson2 * fmom1;
        int fs = fson1 - fson2;
        if (fs == 0) {
            System.out.println("0");
        } else {
            int g = gcd(fs, fm);
            fs = fs / g;
            fm = fm / g;
            System.out.println(fs + "/" + fm);
        }


    }

    public void fMul(int fson1, int fmom1, int fson2, int fmom2) {
        int fm = fmom1 * fmom2;
        int fs = fson1 * fson2;
        if (fs == 0) {
            System.out.println("0");
        } else {
            int g = gcd(fs, fm);
            fs = fs / g;
            fm = fm / g;
            System.out.println(fs + "/" + fm);
        }

    }

    public void fDiv(int fson1, int fmom1, int fson2, int fmom2) {
        int fm = fmom1 * fson2;
        int fs = fson1 + fmom2;
        if (fs == 0) {
            System.out.println("0");
        } else {
            int g = gcd(fs, fm);
            fs = fs / g;
            fm = fm / g;
            System.out.println(fs + "/" + fm);
        }
    }

}

