package com.demo.test;

import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Demo4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = 0;
        String type = null;
        String str = null;
        while (scanner.hasNextLine()) {
            count++;
            if (count == 1) {
                type = scanner.nextLine();
            }
            if (count == 2) {
                str = scanner.nextLine();
                switch (type) {
                    case "sum":
                        sum(str);
                        break;
                    case "num":
                        num(str);
                        break;
                    case "id":
                    case "sort":
                }
            }
        }
    }

    private static final Pattern INT_REGEX = Pattern.compile("(\\d+)");
    private static final Pattern FLOAT_REGEX = Pattern.compile("(\\d+\\.\\d+)");

    public static void id(String str) {
        String regex = "([1-9]\\d{5}(18|19|20)\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3}[0-9Xx])|([1-9]\\d{5}\\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\\d{3})";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(str);
        if (m.find()) {
            System.out.println(m.group());
        }

    }

    public static void num(String str) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            char ch = str.charAt(i);
            if (ch >= 'a' && ch <= 'z') {

            } else if (ch >= 'A' && ch <= 'Z') {

            } else if (ch >= 48 && ch <= 57) {

            } else {
                count++;
            }
        }
        System.out.println(count);

    }

    public static void sum(String str) {
        String result = "";
        Matcher m = INT_REGEX.matcher(str);
        if (m.find()) {
            Map<Integer, String> map = new TreeMap<>();
            m = FLOAT_REGEX.matcher(str);
            while (m.find()) {
                result = m.group(1) == null ? "" : m.group(1);
                int i = str.indexOf(result);
                String s = str.substring(i, i + result.length());
                map.put(i, s);
                str = str.substring(0, i) + str.substring(i + result.length());
            }
            m = INT_REGEX.matcher(str);
            while (m.find()) {
                result = m.group(1) == null ? "" : m.group(1);
                int i = str.indexOf(result);
                if (i != 0 && String.valueOf(str.charAt(i - 1)).equals(".")) {
                    str = str.substring(0, i - 1) + str.substring(i + result.length());
                    continue;
                }
                String s = str.substring(i, i + result.length());
                map.put(i, s);
                str = str.substring(0, i) + str.substring(i + result.length());
            }
            result = "";
            for (Map.Entry<Integer, String> e : map.entrySet()) {
                result += e.getValue() + ",";
            }
            result = result.substring(0, result.length() - 1);
        } else {
            result = "";
        }
        double sum = 0d;
        String[] strs = result.split(",");
        for (String s : strs) {
            sum += Double.parseDouble(s);
        }
        System.out.println(sum);
    }

}
