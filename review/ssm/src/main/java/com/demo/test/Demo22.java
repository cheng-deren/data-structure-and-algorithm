package com.demo.test;

import java.util.Scanner;

public class Demo22 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            String[] str = line.split(" ");
            System.out.println(lastRemaining(Integer.parseInt(str[0]), Integer.parseInt(str[1])));
        }
    }

    public static int lastRemaining(int n, int m) {
        int res = 0;
        for (int i = 2; i <= n; i++) {
            res = (res + m) % i;
        }
        return res + 1;
    }
}
