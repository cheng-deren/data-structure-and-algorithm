package com.demo.test;

import java.util.Scanner;

public class ScannerDemo4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        //判断sc中是否有下一个输入项，多个输入项以空格分开
        while(sc.hasNext()) {
            String s1 = sc.next();
            System.out.println("s1="+s1);
        }
    }
}
