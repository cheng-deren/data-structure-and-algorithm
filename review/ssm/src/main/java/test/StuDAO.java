package test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class StuDAO {
    private String sno;
    private String sname;
    private Integer age;

    public String getSno() {
        return sno;
    }

    public void setSno(String sno) {
        this.sno = sno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public StuDAO queryById(String sno) throws Exception {
        String driverClassName = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306/sc";
        String username = "root";
        String password = "mysql";
        Class.forName(driverClassName);
        Connection conn = DriverManager.getConnection(url, username, password);
        String sql = "select * from student where Sno = ?";
        PreparedStatement stat = conn.prepareStatement(sql);
        stat.setString(1, sno);
        ResultSet rs = stat.executeQuery(sql);
        if (rs.next()) {
            StuDAO stuDAO = new StuDAO();
            stuDAO.setSname(rs.getString("sname"));
            stuDAO.setSno(rs.getString("sno"));
            stuDAO.setAge(rs.getInt("age"));
            return stuDAO;
        }
        return null;
    }
}
