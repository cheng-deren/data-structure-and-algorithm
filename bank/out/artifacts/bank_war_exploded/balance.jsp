<%@ page import="com.company.dao.BankDaoImpl" %>
<%@ page import="com.company.entity.UserInfo" %>
<%@ page import="java.awt.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<%
    request.setCharacterEncoding("UTF-8");
    response.setCharacterEncoding("UTF-8");
    String userID = request.getParameter("userID");
    if (userID == null || "".equals(userID)) {
        response.getWriter().write("请输入的账户ID");
        return;
    }
    BankDaoImpl bankDao = new BankDaoImpl();
    UserInfo userInfo = bankDao.queryById(userID);
    if (userInfo.getBalance() < 0) {
        request.getRequestDispatcher("dept.jsp").forward(request, response);
    }
%>
<table border="1">
    <tr>
        <td>账户ID：</td>
        <td><%=userInfo.getUserId()%>
        </td>
    </tr>
    <tr>
        <td>姓名：</td>
        <td><%=userInfo.getUserName()%>
        </td>
    </tr>
    <tr>
        <td>账户余额：</td>
        <td><%=userInfo.getBalance()%>
        </td>
    </tr>
</table>


</body>
</html>
