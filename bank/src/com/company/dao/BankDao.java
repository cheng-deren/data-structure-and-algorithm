package com.company.dao;

import com.company.entity.UserInfo;

import java.sql.SQLException;


public interface BankDao {

    /**
     *  根据 id 查询用户信息
     * @param id
     * @return
     * @throws SQLException
     * @throws ClassNotFoundException
     */
    public UserInfo queryById(String id) throws SQLException, ClassNotFoundException;
}
