package com.company.dao;


import com.company.entity.UserInfo;

import java.sql.*;

public class BankDaoImpl implements BankDao {
    @Override
    public UserInfo queryById(String id) {

        UserInfo user = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://192.168.80.216:3306/sys?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=GMT%2B8";
            String username = "root";
            String password = "hxd@2021";


            connection = DriverManager.getConnection(url, username, password);

            String sql = "select * from userInfo where userID = ?";
            statement = connection.prepareStatement(sql);
            statement.setObject(1, id);
            resultSet = statement.executeQuery();

            if (resultSet.next()) {
                user = new UserInfo();
                user.setUserId(resultSet.getString("userID"));
                user.setUserName(resultSet.getString("userName"));
                user.setBalance(resultSet.getDouble("balance"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return user;
    }
}
