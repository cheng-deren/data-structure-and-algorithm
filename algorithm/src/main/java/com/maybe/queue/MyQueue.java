package com.maybe.queue;


import java.util.Iterator;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyQueue
 * @date 2022/10/9 18:39
 * @Description
 */
public class MyQueue<T> implements Iterable<T> {
    private Node<T> head;
    private Node<T> last;
    private Integer size;

    public MyQueue() {
        head = new Node(null, null);
        last = null;
        size = 0;
    }

    private class Node<T> {
        private T item;
        private Node<T> next;

        public Node(T item, Node<T> next) {
            this.item = item;
            this.next = next;
        }
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    /**
     * 插入元素
     *
     * @param t
     */
    public void enqueue(T t) {
        if (isEmpty()) {
            last = new Node<T>(t, null);
            head.next = last;
        } else {
            Node<T> oldLast = last;
            last = new Node<>(t, null);
            oldLast.next = last;
        }
        size++;
    }

    /**
     * 删除元素
     *
     * @return
     */
    public T dequeue() {
        if (isEmpty()) {
            return null;
        }
        Node<T> oldFist = head.next;
        head = oldFist.next;

        if (isEmpty()) {
            last = null;
        }
        size--;
        return oldFist.item;
    }


    @Override
    public Iterator iterator() {
        return new Qiterator();
    }

    private class Qiterator implements Iterator<T> {
        private Node<T> fist = head;

        @Override
        public boolean hasNext() {
            return fist.next != null;
        }

        @Override
        public T next() {
            Node<T> current = fist.next;
            //移动下标
            fist = fist.next;
            return current.item;
        }
    }
}
