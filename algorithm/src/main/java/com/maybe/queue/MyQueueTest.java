package com.maybe.queue;

import java.util.Queue;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyQueueTest
 * @date 2022/10/9 18:51
 * @Description
 */
public class MyQueueTest {
    public static void main(String[] args) throws Exception {
        MyQueue<String> queue = new MyQueue<>();
        queue.enqueue("a");
        queue.enqueue("b");
        queue.enqueue("c");
        queue.enqueue("d");
        queue.forEach(System.out::println);
        System.out.println("-----------------------------");
        String result = queue.dequeue();
        System.out.println("出列了元素：" + result);
        System.out.println(queue.size());
    }
}
