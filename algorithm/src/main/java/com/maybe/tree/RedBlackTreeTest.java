package com.maybe.tree;

/**
 * @author cdr
 * @version 1.0
 * @ClassName RedBlackTreeTest
 * @date 2022/11/1 15:20
 * @Description 红黑树测试
 */
public class RedBlackTreeTest {
    public static void main(String[] args) {
        RedBlackTree<Integer, String> bt = new RedBlackTree<>();
        bt.put(1, "张三");
        bt.put(3, "李四");
        bt.put(4, "二哈");
        bt.put(5, "王五");
        bt.put(2, "老二");
        bt.put(0, "零");
        System.out.println(bt.size());
        bt.put(1,"老三");
        System.out.println(bt.get(1));
        System.out.println(bt.get(3));
        System.out.println(bt.get(4));
        System.out.println(bt.get(5));

    }
}
