package com.maybe.tree;


import javax.swing.*;

/**
 * @author cdr
 * @version 1.0
 * @ClassName RedBlackTree
 * @date 2022/10/27 16:21
 * @Description 红黑树
 */
public class RedBlackTree<K extends Comparable<K>, V> {

    private Node root;
    private int size;
    private static final boolean RED = true;
    private static final boolean BLACK = false;

    private class Node {
        private K key;
        private V value;
        private Node left;
        private Node right;
        private boolean color;

        public Node(K key, V value, Node left, Node right, boolean color) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
            this.color = color;
        }

    }

   /* public RedBlackTree() {
        Node node = new Node(null, null, null, null, BLACK);
        root = node;
        size = 0;
    }*/

    public int size() {
        return size;
    }

    /**
     * 判断节点颜色是否是红色
     *
     * @param current
     * @return
     */
    private boolean isRed(Node current) {
        if (current == null) {
            return false;
        }
        return current.color;
    }

    /**
     * 判断节点颜色是否是红色
     *
     * @param current
     * @return
     */
    private boolean isBlack(Node current) {
        if (current == null) {
            return true;
        }
        return current.color;
    }


    /**
     * 左旋：
     * 以某个节点作为支点（fulcrum）
     * 支点的下一个节点 作为旋转点 （current）
     * 1.fulcrum 右子节点 = current 左子节点
     * 2.current 左子节点 = fulcrum 节点（current 变为 fulcrum 节点的父节点）
     * 3.current 的颜色 = fulcrum 的颜色
     * 4.fulcrum 的颜色 = 红色
     * TODO 左旋： 支点的右子节点 = 旋转点的左子节点 并重置 旋转点的左子节点 = 支点
     *
     * @param fulcrum 支点
     * @return
     */
    private Node rotateLeft(Node fulcrum) {
        Node current = fulcrum.right;
        fulcrum.right = current.left;
        current.left = fulcrum;
        current.color = fulcrum.color;
        fulcrum.color = RED;
        return current;
    }

    /**
     * 右旋：
     * 以某个节点作为支点（fulcrum）
     * 支点的下一个节点 作为旋转点 （current）
     * 1.fulcrum 左子节点 = current 右子节点
     * 2.current 右子节点 = fulcrum 节点
     * 3.current 的颜色 = fulcrum 的颜色
     * 4.fulcrum 的颜色 = 红色
     * TODO 右旋：支点的左子节点 = 旋转点的右子节点 并重置 旋转点的右子节点 = 支点
     *
     * @param fulcrum 支点
     * @return
     */
    private Node rotateRight(Node fulcrum) {
        Node current = fulcrum.left;
        fulcrum.left = current.right;
        current.right = fulcrum;
        current.color = fulcrum.color;
        fulcrum.color = RED;
        return current;
    }

    /**
     * 颜色反转：
     * 父节点：fulcrum
     * 1.父节点改成红色
     * 2.父节点左子节点改成黑色
     * 3.父节点右子节点改成黑色
     *
     * @param fulcrum
     */
    private void flipColors(Node fulcrum) {
        if (fulcrum == null) {
            throw new RuntimeException("fulcrum is null");
        }
        fulcrum.color = RED;
        fulcrum.left.color = BLACK;
        fulcrum.right.color = BLACK;
    }

    /**
     * 插入方法
     */
    public void put(K key, V value) {
        //在整个树上插入元素
        root = put(root, key, value);
        //根节点颜色默认为黑色
        root.color = BLACK;
    }

    /**
     * 指定节点插入元素
     *
     * @param root
     * @param key
     * @param value
     * @return
     */
    private Node put(Node root, K key, V value) {
        if (root == null) {
            size++;
            //标准插入，第一个节点红色
            return new Node(key, value, null, null, RED);
        }
        switch (key.compareTo(root.key)) {
            case -1:
                root.left = put(root.left, key, value);
                break;
            case 1:
                root.right = put(root.right, key, value);
                break;
            case 0:
                root.value = value;
                break;
        }
        //平衡化
        if (isRed(root.right) && isBlack(root.left)) {
            root = rotateLeft(root);
        }
        if (isRed(root.left) && isRed(root.left.left)) {
            root = rotateRight(root);
        }
        if (isRed(root.left) && isRed(root.right)) {
            flipColors(root);
        }
        return root;
    }

    /**
     * 根据 key 获取元素
     *
     * @param key
     * @return
     */
    public V get(K key) {
        //从整个树里面获取
        return get(root, key);
    }

    private V get(Node root, K key) {
        if (root == null || key == null) {
            return null;
        }
        switch (key.compareTo(root.key)) {
            case -1:
                return get(root.left, key);
            case 1:
                return get(root.right, key);
        }

        return root.value;
    }


}
