package com.maybe.tree;

import java.util.ArrayDeque;

/**
 * @author cdr
 * @version 1.0
 * @ClassName BinaryTree
 * @date 2022/10/12 10:25
 * @Description 二叉查找树API设计
 */
public class BinaryTree<K extends Comparable<K>, V> {

    /**
     * 根节点
     */
    private Node<K, V> root;

    private int size;

    /**
     * Node 节点实现
     *
     * @param <K>
     * @param <V>
     */
    private class Node<K, V> {
        private K key;
        private V value;
        private Node<K, V> left;
        private Node<K, V> right;

        public Node(K key, V value, Node<K, V> left, Node<K, V> right) {
            this.key = key;
            this.value = value;
            this.left = left;
            this.right = right;
        }

    }

    public int size() {
        return size;
    }

    public boolean isEmpty() {
        return size == 0;
    }


    /**
     * 插入
     */
    public void put(K key, V value) {
        root = put(root, key, value);
    }

    /**
     * @param node  插入时 右为尊，大的子节点在右边，小的在左边
     * @param key
     * @param value
     * @return
     */
    private Node<K, V> put(Node<K, V> node, K key, V value) {
        if (key == null) {
            throw new RuntimeException("key is null");
        }
        if (node == null) {
            size++;
            return new Node<K, V>(key, value, null, null);
        }
        int to = key.compareTo(node.key);
        switch (to) {
            case -1:
                node.left = put(node.left, key, value);
                break;
            case 1:
                node.right = put(node.right, key, value);
                break;
            case 0:
                node.value = value;
                break;
        }

        return node;
    }


    /**
     * 获取 value
     *
     * @param key
     * @return
     */
    public V get(K key) {
        return get(root, key);
    }

    private V get(Node<K, V> node, K key) {
        if (node == null) {
            return null;
        }
        V temp = null;
        int to = key.compareTo(node.key);
        switch (to) {
            case -1:
                return get(node.left, key);
            case 1:
                return get(node.right, key);
            case 0:
                temp = node.value;
        }
        return temp;
    }

    public void delete(K key) {
        root = delete(root, key);
    }

    /**
     * 实现思路
     * 1.找到被删除结点；
     * 2.找到被删除结点右子树中的最小结点minNode
     * 3.删除右子树中的最小结点
     * 4.让被删除结点的左子树称为最小结点minNode的左子树，让被删除结点的右子树称为最小结点minNode的右子树
     * 5.让被删除结点的父节点指向最小结点minNode
     */
    private Node<K, V> delete(Node<K, V> node, K key) {
        if (node == null) {
            return null;
        }
        int to = key.compareTo(node.key);
        switch (to) {
            case -1:
                node.left = delete(node.left, key);
                break;
            case 1:
                node.right = delete(node.right, key);
                break;
            case 0:
                //当前 node.key == key，那么就是需要删除的结点
                size--;
                //1.如果当前结点的左子树不存在，则直接返回当前结点的右子结点
                if (node.left == null) {
                    return node.right;
                }
                //2.如果当前结点的右子树不存在，则直接返回当前结点的左子结点
                if (node.right == null) {
                    return node.left;
                }

                //3.当前结点的左右子树都存在，找到右子树中最小的结点
                Node<K, V> minNode = node.right;
                while (minNode.left != null) {
                    minNode = minNode.left;
                }

                //4.删除 minNode 结点连接
                Node<K, V> tempNode = node.right;
                while (tempNode.left != null) {
                    if (tempNode.left.left == null) {
                        tempNode.left = null;
                    } else {
                        //变换结点位置
                        tempNode = tempNode.left;
                    }
                }

                //5.让被删除结点的左子树称为最小结点minNode的左子树，让被删除结点的右子树称为最小结点minNode的右子树
                minNode.left = node.left;
                minNode.right = node.right;

                //6.让被删除结点的父节点指向最小结点minNode
                node = minNode;
                break;
        }
        return node;
    }

    /**
     * 获取最小键
     *
     * @return
     */
    public K getMin() {
        return getMin(root).key;
    }

    private Node<K, V> getMin(Node<K, V> node) {
        if (node == null) {
            return null;
        }
        if (node.left != null) {
            return getMin(node.left);
        }
        return node;
    }

    /**
     * 获取最大键
     *
     * @return
     */
    public K getMax() {
        return getMax(root).key;
    }

    private Node<K, V> getMax(Node<K, V> node) {
        if (node == null) {
            return null;
        }
        if (node.right != null) {
            return getMax(node.right);
        }
        return node;
    }

    /**
     * 前置遍历，根结点，左结点，右结点
     *
     * @return
     */
    public ArrayDeque<K> preorderTraversal() {
        ArrayDeque<K> deque = new ArrayDeque<>();
        //遍历数据
        preorderTraversal(root, deque);
        return deque;
    }

    private void preorderTraversal(Node<K, V> node, ArrayDeque<K> deque) {
        if (node == null) {
            return;
        }
        //插入数据（根结点）
        deque.add(node.key);

        //左结点
        if (node.left != null) {
            preorderTraversal(node.left, deque);
        }
        //右结点
        if (node.right != null) {
            preorderTraversal(node.right, deque);
        }
    }

    /**
     * 中序遍历，左结点，根结点，右结点
     *
     * @return
     */
    public ArrayDeque<K> middleTraversal() {
        ArrayDeque<K> deque = new ArrayDeque<>();
        middleTraversal(root, deque);
        return deque;
    }

    private void middleTraversal(Node<K, V> node, ArrayDeque<K> deque) {
        if (node == null) {
            return;
        }
        //左结点
        if (node.left != null) {
            middleTraversal(node.left, deque);
        }
        //插入数据（根结点）
        deque.add(node.key);
        //右结点
        if (node.right != null) {
            middleTraversal(node.right, deque);
        }
    }

    /**
     * 后置遍历，左结点，右节点，根结点
     *
     * @return
     */
    public ArrayDeque<K> postTraversal() {
        ArrayDeque<K> deque = new ArrayDeque<>();
        postTraversal(root, deque);
        return deque;
    }

    private void postTraversal(Node<K, V> node, ArrayDeque<K> deque) {
        if (node == null) {
            return;
        }
        //左结点
        if (node.left != null) {
            postTraversal(node.left, deque);
        }
        //右结点
        if (node.right != null) {
            postTraversal(node.right, deque);
        }
        //插入数据（根结点）
        deque.add(node.key);
    }

    /**
     * 层序遍历，从上往下，从左往右，遍历
     *
     * @return
     */
    public ArrayDeque<K> sequenceTraversal() {
        ArrayDeque<K> keyDeque = new ArrayDeque<K>();
        ArrayDeque<Node<K, V>> nodeDeque = new ArrayDeque<>();

        if (root != null) {
            //默认放入根节点
            nodeDeque.add(root);
        }
        while (!nodeDeque.isEmpty()) {
            Node<K, V> current = nodeDeque.poll();
            keyDeque.add(current.key);

            if (current.left != null) {
                nodeDeque.add(current.left);
            }
            if (current.right != null) {
                nodeDeque.add(current.right);
            }
        }
        return keyDeque;
    }

    /**
     * 最大深度
     *
     * @return
     */
    public int maximumDepth() {
        return maximumDepth(root);
    }

    private int maximumDepth(Node<K, V> node) {
        if (node == null) {
            return 0;
        }
        int maxLeft = 0;
        int maxRight = 0;

        if (node.left != null) {
            maxLeft = maximumDepth(node.left);
        }
        if (node.right != null) {
            maxRight = maximumDepth(node.right);
        }
        return Math.max(maxLeft, maxRight) + 1;
    }

}
