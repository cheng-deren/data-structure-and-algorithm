package com.maybe.tree;

import java.util.ArrayDeque;

/**
 * @author cdr
 * @version 1.0
 * @ClassName BinaryTreeTest
 * @date 2022/10/13 9:27
 * @Description
 */
public class BinaryTreeTest {

    public static void main(String[] args) throws Exception {
        BinaryTree<Integer, String> bt = new BinaryTree<>();
        bt.put(15, "爱情");

        bt.put(6, "二哈");
        bt.put(5, "张三");
        bt.put(8, "李四");

        bt.put(20, "王五");
        bt.put(19, "二哈");
        bt.put(18, "李四");
        bt.put(27, "张三");

        System.out.println(bt.size());
        //测试删除数据
        //bt.delete(15);
        System.out.println(bt.size());
        System.out.println("最小键：" + bt.getMin());
        System.out.println("最大键：" + bt.getMax());
        System.out.println("--------------------------遍历数据------------------------------");
        ArrayDeque<Integer> preorderDeque = bt.preorderTraversal();
        System.out.println("前序遍历：" + preorderDeque.toString());

        ArrayDeque<Integer> middleDeque = bt.middleTraversal();
        System.out.println("中序遍历：" + middleDeque.toString());

        ArrayDeque<Integer> postDeque = bt.postTraversal();
        System.out.println("后序遍历：" + postDeque.toString());

        ArrayDeque<Integer> sequenceDeque = bt.sequenceTraversal();
        System.out.println("层序遍历：" + sequenceDeque.toString());

        System.out.println("最大深度：" + bt.maximumDepth());

    }

}
