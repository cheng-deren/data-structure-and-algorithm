package com.maybe.stack;

import javax.management.Query;
import java.util.Objects;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingDeque;

/**
 * @author cdr
 * @version 1.0
 * @ClassName StackDemo
 * @date 2022/10/8 13:54
 * @Description TODO  括号匹配问题
 */
public class StackDemo {
    public static void main(String[] args) {
        String str = "(fdafds(fafds)())";
        boolean match = isMatch(str);
        System.out.println(str + "中的括号是否匹配：" + match);

    }

    private static boolean isMatch(String str) {
        MyStackByNode<String> myStack = new MyStackByNode<>();
        if (Objects.nonNull(str)) {
            for (int i = 0; i < str.length(); i++) {
                String current = String.valueOf(str.charAt(i));
                if (Objects.equals("(", current)) {
                    myStack.push(current);
                }
                if (Objects.equals(")", current)) {
                    String pop = myStack.pop();
                    if (Objects.isNull(pop)) {
                        return false;
                    }
                }
            }
            if (!myStack.isEmpty()) {
                return false;
            }
        }
        return true;
    }
}
