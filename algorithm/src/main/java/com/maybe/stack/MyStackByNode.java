package com.maybe.stack;

import java.util.Iterator;
import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyStack
 * @date 2022/8/20 17:45
 * @Description 栈（采用 链表实现）
 */
public class MyStackByNode<T> implements Iterable {

    private Node<T> head;
    private int size;

    public MyStackByNode() {
        this.head = new Node<>(null, null);
        this.size = 0;
    }

    @Override
    public Iterator iterator() {
        return new SIterator();
    }

    private class SIterator implements Iterator<T> {
        private Node current = head;

        @Override
        public boolean hasNext() {
            return current.next != null;
        }

        @Override
        public T next() {
            current = current.next;
            return (T) current.item;
        }
    }

    private class Node<T> {
        private T item;
        private Node<T> next;

        public Node(T item, Node<T> next) {
            this.item = item;
            this.next = next;
        }

    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int size() {
        return size;
    }

    /**
     * 向栈中压入元素 T
     * 每次压栈 都将新的元素放到 head 的 下一个结点（也就是链表处 head 第一个结点）
     * 这样弹栈的时候，每次就弹第一个结点，即可实现先进后出（FILO）
     *
     * @param t
     */
    public void push(T t) {
        //原本（第一个结点）
        Node old = head.next;
        Node<T> newNode = new Node<>(t, null);
        //新结点变成第一个结点
        head.next = newNode;
        //新结点下一个指向之前结点
        newNode.next = old;
        size++;
    }


    /**
     * 弹出栈顶元素
     *
     * @return
     */
    public T pop() {
        //原本（第一个结点）
        Node old = head.next;
        //old == null 那么就说明最后一个结点
        if (Objects.nonNull(old)) {
            //跟新结点，原本结点若不为空，那么将被删除，所以直接将头结点指向原结点下一个
            head.next = old.next;
        }
        size--;
        return (T) old.item;
    }

}
