package com.maybe.stack;


/**
 * @author cdr
 * @version 1.0
 * @ClassName MyStack
 * @date 2022/8/20 17:45
 * @Description 栈（采用 链表实现）
 */
public class MyStackByNodeTest {
    public static void main(String[] args) {
        MyStackByNode<Integer> stackByNode = new MyStackByNode();
        stackByNode.push(1);
        stackByNode.push(2);
        stackByNode.push(3);
        stackByNode.push(4);

        //压栈
        stackByNode.forEach(System.out::println);
        System.out.println("------------------------");

        //弹栈
        System.out.println("弹栈元素：" + stackByNode.pop());
        System.out.println("栈大小：" + stackByNode.size());
        System.out.println("------------------------");


    }

}
