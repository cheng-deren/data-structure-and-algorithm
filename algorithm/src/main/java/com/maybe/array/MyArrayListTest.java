package com.maybe.array;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyArrayListTest
 * @date 2022/8/14 15:37
 * @Description
 */
public class MyArrayListTest {
    public static void main(String[] args) {
        MyArrayList<String> list = new MyArrayList<String>(20);
        list.insert("张三");
        list.insert("李四");
        list.insert("王五");

        //指定插入
        list.insert(2, "王子");
        System.out.println(list.size());

        //指定删除
        list.remove(2);
        System.out.println(list.size());
        list.forEach(System.out::println);

        //获取数据
        System.out.println(list.indexOf("王五"));


    }
}
