package com.maybe.array;

import java.util.Iterator;
import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyArrayList
 * @date 2022/8/13 13:30
 * @Description 顺序表实现（有序列表）
 * 特征：
 * 1. 第一个数据元素没有前驱，这个数据元素被称为头结点；
 * 2. 最后一个数据元素没有后继，这个数据元素被称为尾结点；
 * 3. 除了第一个和最后一个数据元素外，其他数据元素有且仅有一个前驱和一个后继
 */
public class MyArrayList<T> implements Iterable<T> {
    /**
     * 存储元素的数组
     */
    private T[] element;

    private T[] elementCopy;
    /**
     * 当前线性表的长度
     */
    private int size;

    public MyArrayList(int capacity) {
        element = (T[]) new Object[capacity];
        this.size = 0;
    }


    /**
     * 校验下标
     */
    private void check(int index) {
        if (index >= size || index < 0) {
            throw new RuntimeException("下标越界");
        }
    }

    /**
     * 获取线性表中元素的个数
     *
     * @return
     */
    public int size() {
        return size;
    }

    /**
     * 获取指定索引的元素
     *
     * @param index
     * @return
     */
    public T get(int index) {
        check(index);
        return element[index];
    }


    /**
     * 在指定索引处插入元素
     *
     * @param index
     * @param data
     */
    public void insert(int index, T data) {
        expansion();
        for (int i = size; i > index; i--) {
            element[i] = element[i - 1];
        }
        element[index] = data;
        size++;
    }

    /**
     * 向线性表中添加一个元素t
     *
     * @param t
     */
    public void insert(T t) {
        expansion();
        element[size++] = t;
    }

    /**
     * 删除并返回线性表中第i个数据元素。
     *
     * @return
     */
    public T remove(int index) {
        expansion();
        T t = get(index);
        for (int i = size - 1; i > index; i--) {
            element[i - 1] = element[i];
        }
        size--;

        //判断是否需要缩容
        int threshold = element.length / 4;
        if (size < element.length / 4) {
            elementCopy = (T[]) new Object[threshold];
            for (int i = 0; i < size; i++) {
                elementCopy[i] = element[i];
            }
            element = elementCopy;
        }
        return t;
    }

    /**
     * 返回线性表中首次出现的指定的数据元素的位序号，
     * 若不存在，则返回-1
     *
     * @return
     */
    public int indexOf(T t) {
        for (int i = 0; i < size; i++) {
            if (Objects.equals(element[i], t)) {
                return i;
            }
        }
        return -1;
    }


    /**
     * 判断是否需要扩容
     */
    private void expansion() {
        if (size < element.length) {
            return;
        }
        elementCopy = (T[]) new Object[element.length * 2];
        for (int i = 0; i < element.length; i++) {
            elementCopy[i] = element[i];
        }

        element = elementCopy;

    }


    /**
     * 实现遍历
     *
     * @return
     */
    @Override
    public Iterator<T> iterator() {
        return new MyArrayListIterator();
    }

    private class MyArrayListIterator implements Iterator {
        private int current;

        public MyArrayListIterator() {
            current = 0;
        }

        /**
         * 判断是否还有下一个元素
         *
         * @return
         */
        @Override
        public boolean hasNext() {
            return current < size;
        }

        /**
         * 获取下一个元素
         *
         * @return
         */
        @Override
        public Object next() {
            return element[current++];
        }
    }


}
