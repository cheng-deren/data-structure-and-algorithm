package com.maybe.sort;


/**
 * @author cdr
 * @version 1.0
 * @ClassName 递归排序
 * @date 2022/10/13 10:22
 * @Description
 */
public class 递归排序 {

    public static void main(String[] args) {
        long result = factorial(5);
        System.out.println(result);
    }

    /**
     * 求 n 的阶乘
     *
     * @param n
     * @return
     */
    private static long factorial(int n) {
        if (n == 1) {
            return 1;
        }
        return n * factorial(n - 1);
    }
}
