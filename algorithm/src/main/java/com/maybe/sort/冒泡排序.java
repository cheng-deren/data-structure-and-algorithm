package com.maybe.sort;

import java.util.Arrays;

/**
 * @author cdr
 * @version 1.0
 * @ClassName 冒泡排序
 * @date 2022/8/11 21:18
 * @Description 冒泡排序
 * 1. 比较相邻的元素。如果前一个元素比后一个元素大，就交换这两个元素的位置。（需要比较 N-1 次）
 * 2. 对每一对相邻元素做同样的工作，从开始第一对元素到结尾的最后一对元素。最终最后位置的元素就是最大
 * 值。
 */
@SuppressWarnings(value = "ALL")
public class 冒泡排序 {
    public static void main(String[] args) {
        int[] nums = {4, 5, 6, 3, 2, 1};
        //nums.length - 1；需要冒泡的次数
        for (int i = 0; i < nums.length - 1; i++) {
            //nums.length - 1 - i；每一次需要从第一位比到最后一位，但是比较过的最后 i 位元素就没必要再比较
            for (int j = 0; j < nums.length - 1 - i; j++) {
                if (nums[j] > nums[j + 1]) {
                    int temp = nums[j];
                    nums[j] = nums[j + 1];
                    nums[j + 1] = temp;
                }
            }
        }

        System.out.println(Arrays.toString(Arrays.stream(nums).toArray()));
    }

}
