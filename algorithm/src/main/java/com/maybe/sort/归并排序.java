package com.maybe.sort;

/**
 * @author cdr
 * @version 1.0
 * @ClassName 归并排序
 * @date 2022/10/13 10:40
 * @Description
 */
@SuppressWarnings(value = "ALL")
public class 归并排序 {
    /**
     * 归并所需要的辅助数组
     */
    private static Comparable[] assist;


    /**
     * 比较 a元素 是否小于 b元素
     */
    private static boolean less(Comparable a, Comparable b) {
        return a.compareTo(b) == -1;
    }

}
