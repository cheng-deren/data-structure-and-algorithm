package com.maybe.sort;

import java.util.Arrays;

/**
 * @author cdr
 * @version 1.0
 * @ClassName 选择排序
 * @date 2022/8/11 21:36
 * @Description 选择排序
 * 1.每一次遍历的过程中，都假定第一个索引处的元素是最小值，和其他索引处的值依次进行比较，如果当前索引处
 * 的值大于其他某个索引处的值，则假定其他某个索引出的值为最小值，最后可以找到最小值所在的索引
 * 2.交换第一个索引处和最小值所在的索引处的值
 */
@SuppressWarnings(value = "ALL")
public class 选择排序 {

    public static void main(String[] args) {
        int[] nums = {4, 6, 8, 7, 9, 2, 10, 1};

        //需要比较 N-1 次
        for (int i = 0; i < nums.length - 1; i++) {
            //假设 索引 i 的值最小
            int minIndex = i;
            //j = i + 1，每次排序完，交换第一个索引处和最小值所在的索引处的值（minIndex索引）加 i 为了过滤掉已经排序的
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[minIndex] > nums[j]) {
                    minIndex = j;
                }
            }
            //交换第一个索引处和最小值所在的索引处的值
            int temp = nums[i];
            nums[i] = nums[minIndex];
            nums[minIndex] = temp;
        }

        System.out.println(Arrays.toString(Arrays.stream(nums).toArray()));
    }

}
