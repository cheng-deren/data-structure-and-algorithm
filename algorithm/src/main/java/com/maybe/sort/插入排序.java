package com.maybe.sort;

import java.util.Arrays;

/**
 * @author cdr
 * @version 1.0
 * @ClassName 插入排序
 * @date 2022/8/11 21:36
 * @Description 插入排序
 * 1.把所有的元素分为两组，已经排序的和未排序的；（默认第一个元素就是已经排序的）
 * 2.找到未排序的组中的第一个元素，向已经排序的组中进行插入；
 * 3.倒叙遍历已经排序的元素，依次和待插入的元素进行比较，直到找到一个元素小于等于待插入元素，那么就把待
 * 插入元素放到这个位置（break），其他的元素向后移动一位；
 */
@SuppressWarnings(value = "ALL")
public class 插入排序 {

    public static void main(String[] args) {
        int[] nums = {4, 3, 2, 10, 12, 1, 5, 6};

        /**
         * 1.默认第一个元素已排序，跳过第一个（index = 0 的元素）！
         */
        //默认 0 已排序 ，那么 index > 0 就是未排序第一个
        for (int i = 1; i < nums.length; i++) {
            /**
             * 1.已知：index < i 的元素就是 "已排序元素" 那么 i 就是未排序数据第一个元素咯。
             * 2.j = i，拿到 未排序 第一个元素 和 已排序的依次比较
             */
            // index < i （j）的元素，就是已排序的最后一个
            for (int j = i; j > 0; j--) {
                /**
                 * 1.拿 "未排序" 第一个元素（j），和 已排序 的元素进行比较
                 * 2.已知：j 是 "未排序" 第一个元素，那么 j - 1 就是 "已排序元素" 的最后一个呀
                 * 2.如果未排序的元素都大于 "已排序元素" 前面都是已排序的数据你比最后一个都大（那么就默认当前位置，）
                 */
                if (nums[j] > nums[j - 1]) {
                    break;
                }
                int temp = nums[j];
                nums[j] = nums[j - 1];
                nums[j - 1] = temp;
            }
        }

        System.out.println(Arrays.toString(Arrays.stream(nums).toArray()));
    }

}
