package com.maybe.map;

import com.sun.jdi.Value;

import java.util.Iterator;
import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName SymbolTable
 * @date 2022/10/10 10:34
 * @Description 符号表 （k-v）
 */
public class MyHashMap<K, V> implements Iterable<K> {

    private Node head;
    private int size;

    public MyHashMap() {
        this.head = new Node(null, null, null);
        this.size = 0;
    }

    private class Node {
        private K key;
        private V value;
        private Node next;

        public Node(K key, V value, Node next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }


    public int size() {
        return size;
    }


    public boolean isEmpty() {
        return size == 0;
    }


    /**
     * 插入元素
     *
     * @param key
     * @param value
     */
    public void put(K key, V value) {
        Node current = head;
        //key 存在将进行覆盖，覆盖并不算新增
        while (current.next != null) {
            current = current.next;
            if (Objects.equals(current.key, key)) {
                current.value = value;
                return;
            }
        }

        Node oldFist = head.next;
        Node newNode = new Node(key, value, oldFist);
        head.next = newNode;

        size++;
    }

    /**
     * 删除元素
     *
     * @param key
     */
    public void delete(K key) {
        if (Objects.isNull(key)) {
            throw new RuntimeException("key is null");
        }
        Node current = head;
        while (current.next != null) {
            if (Objects.equals(current.next.key, key)) {
                current.next = current.next.next;
                size--;
                return;
            }
            //后置移动下标
            current = current.next;
        }
    }

    /**
     * 获取元素
     *
     * @param key
     * @return
     */
    public V get(K key) {
        Node current = head;
        while (current.next != null) {
            current = current.next;
            if (Objects.equals(current.key, key)) {
                return current.value;
            }
        }
        return null;
    }


    @Override
    public Iterator<K> iterator() {
        return null;
    }

}
