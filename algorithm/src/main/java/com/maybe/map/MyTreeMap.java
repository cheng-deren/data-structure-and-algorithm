package com.maybe.map;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName SymbolTable
 * @date 2022/10/10 10:34
 * @Description 有序符号表 （k-v）
 */
public class MyTreeMap<K extends Comparable<K>, V> implements Iterable<K> {

    private Node head;
    private int size;

    public MyTreeMap() {
        this.head = new Node(null, null, null);
        this.size = 0;
    }

    private class Node {
        private K key;
        private V value;
        private Node next;

        public Node(K key, V value, Node next) {
            this.key = key;
            this.value = value;
            this.next = next;
        }
    }


    public int size() {
        return size;
    }


    public boolean isEmpty() {
        return size == 0;
    }


    /**
     * 插入元素，有序插入保证元素顺序，头插法
     *
     * @param key
     * @param value
     */
    public void put(K key, V value) {
        Node current = head.next;
        Node first = head;

        //找到该 key 存放的位置，a.compareTo(b)，a > b = 1  反之 a < b = -1
        while (current != null && current.key.compareTo(key) < 0) {
            // <  0  为正序
            first = current;
            current = current.next;
        }

        //key 相同覆盖数据
        if (current != null && current.key.compareTo(key) == 0) {
            current.value = value;
            return;
        }

        //插入数据
        Node node = new Node(key, value, current);
        first.next = node;
        size++;

    }

    /**
     * 删除元素
     *
     * @param key
     */
    public void delete(K key) {
        if (Objects.isNull(key)) {
            throw new RuntimeException("key is null");
        }
        Node current = head;
        while (current.next != null) {
            if (Objects.equals(current.next.key, key)) {
                current.next = current.next.next;
                size--;
                return;
            }
            //后置移动下标
            current = current.next;
        }
    }

    /**
     * 获取元素
     *
     * @param key
     * @return
     */
    public V get(K key) {
        Node current = head;
        while (current.next != null) {
            current = current.next;
            if (Objects.equals(current.key, key)) {
                return current.value;
            }
        }
        return null;
    }


    @Override
    public Iterator<K> iterator() {
        return null;
    }

}
