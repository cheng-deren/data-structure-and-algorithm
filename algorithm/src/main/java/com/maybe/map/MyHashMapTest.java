package com.maybe.map;

/**
 * @author cdr
 * @version 1.0
 * @ClassName SymbolTableTest
 * @date 2022/10/10 11:07
 * @Description
 */
public class MyHashMapTest {
    public static void main(String[] args) throws Exception {
        MyHashMap<Integer, String> st = new MyHashMap<>();
        st.put(1, "张三");
        st.put(3, "李四");
        st.put(5, "王五");
        System.out.println(st.size());
        System.out.println(st.get(1));
        System.out.println(st.size());
        st.delete(1);
        System.out.println(st.size());
        System.out.println(st.get(1));
        st.put(5, "5");
        System.out.println(st.get(5));
        System.out.println(st.size());
    }
}
