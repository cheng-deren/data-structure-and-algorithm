package com.maybe.map;

/**
 * @author cdr
 * @version 1.0
 * @ClassName SymbolTableTest
 * @date 2022/10/10 11:07
 * @Description
 */
public class MyTreeMapTest {
    public static void main(String[] args) throws Exception {
        MyTreeMap<Integer, String> bt = new MyTreeMap<>();
        bt.put(5, "二哈");
        bt.put(3, "张三");
        bt.put(2, "李四");
        bt.put(4, "王五");

        Integer a = -5;
        Integer b = 3;
        System.out.println(a.compareTo(b));
        System.out.println(a.compareTo(b) < 0);
    }
}
