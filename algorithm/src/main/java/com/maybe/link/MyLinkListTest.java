package com.maybe.link;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyLinkListTest
 * @date 2022/8/14 16:53
 * @Description
 */
@SuppressWarnings(value = "ALL")
public class MyLinkListTest {
    public static void main(String[] args) {
        MyLinkList<String> list = new MyLinkList<>();
        list.insert("1");
        list.insert("2");
        list.insert("3");
        list.insert("4");

        System.out.println(list.length());
        System.out.println("-------------------");
        //测试get方法
        System.out.println(list.get(2));
        System.out.println("------------------------");
        //测试remove方法
        String remove = list.remove(1);
        System.out.println(remove);

        System.out.println("------------------------");
        System.out.println(list.length());


        System.out.println("------------------------");
        list.insert(1,"2");


        list.forEach(System.out::println);
        System.out.println("------------------------");


        //测试反转
        list.reverse();
        list.forEach(System.out::println);
    }
}
