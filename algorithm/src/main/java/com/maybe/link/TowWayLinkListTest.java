package com.maybe.link;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyLinkListTest
 * @date 2022/8/14 16:53
 * @Description
 */
@SuppressWarnings(value = "ALL")
public class TowWayLinkListTest {
    public static void main(String[] args) {
        TowWayLinkList<String> list = new TowWayLinkList<>();
        list.insert("张三");
        list.insert("李四");
        list.insert("王五");
        list.insert("赵六");


        System.out.println("获取第一个结点:"+list.getFirst());
        System.out.println("测试remove方法,下标 3:" + list.remove(3));
        System.out.println(list.size);
        System.out.println("获取最后一个结点:"+list.getLast());


        System.out.println("-----------------");
        list.insert(1, "难受");
        System.out.println(list.size);
        list.forEach(System.out::println);
    }
}
