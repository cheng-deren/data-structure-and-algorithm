package com.maybe.link;

import java.util.Iterator;
import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyLinkList
 * @date 2022/8/14 16:10
 * @Description 双向链表
 * 双向链表也叫双向表，是链表的一种，它由多个结点组成，每个结点都由一个数据域和两个指针域组成，数据域用
 * 来存储数据，其中一个指针域用来指向其后继结点，另一个指针域用来指向前驱结点。链表的头结点的数据域不存
 * 储数据，指向前驱结点的指针域值为null，指向后继结点的指针域指向第一个真正存储数据的结点。
 */
public class TowWayLinkList<T> implements Iterable<T> {

    public Node first;
    public Node last;
    public int size;


    /**
     * 结点实现
     */
    private class Node<T> {
        public T data;
        public Node pre;
        public Node next;

        public Node(T data, Node pre, Node next) {
            this.data = data;
            this.pre = pre;
            this.next = next;
        }
    }

    /**
     * 链表初始化
     */
    public TowWayLinkList() {
        first = new Node<T>(null, null, null);
        size = 0;
    }


    /**
     * 校验下标
     */
    private void check(int index) {
        if (index >= size || index < 0) {
            throw new RuntimeException("下标越界");
        }
    }

    public T getFirst() {
        return isEmpty() ? null :(T) first.next.data;
    }

    public T getLast() {
        return isEmpty() ? null :(T) last.data;
    }

    public void clear() {
        first.next = null;
        last = null;
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int length() {
        return size;
    }

    /**
     * 读取并返回线性表中的第i个元素的值
     *
     * @param i
     * @return
     */
    public T get(int i) {
        check(i);
        Node<T> item = first.next;
        for (int j = 0; j < i; j++) {
            item = item.next;
        }
        return item.data;
    }

    /**
     * 往线性表中添加一个元素
     *
     * @param t
     */
    public void insert(T t) {
        if (isEmpty()) {
            //链表为空,进来的一个元素就是尾结点
            Node<T> tNode = new Node<>(t, first, null);
            first.next = tNode;
            last = tNode;
        } else {
            Node<T> temp = last;
            Node<T> tNode = new Node<T>(t, temp, null);
            temp.next = tNode;
            last = tNode;
        }
        size++;
    }

    /**
     * 向指定位置i处，添加元素t
     */
    public void insert(int i, T t) {
        check(i);
        Node<T> before = first;
        //找到 i 下标前一个结点
        for (int j = 0; j < i; j++) {
            before = before.next;
        }
        //当前结点
        Node<T> current = before.next;

        //插入结点
        Node<T> tNode = new Node<>(t, before, current);

        before.next = tNode;
        current.pre = tNode;
        size++;
    }

    /**
     * 删除指定位置i处的元素，并返回被删除的元素
     *
     * @param i
     * @return
     */
    public T remove(int i) {
        check(i);
        Node<T> before = first;
        for (int j = 0; j < i; j++) {
            before = before.next;
        }
        //当前结点
        Node<T> current = before.next;
        //后面一个结点
        Node<T> after = current.next;

        //如果删除的就是当前最后一个结点
        if (Objects.isNull(after)) {
            before.next = null;
            current.pre = null;
            last = before;
        } else {
            before.next = after;
            after.pre = before;
        }

        size--;
        return current.data;
    }

    /**
     * 查找元素t在链表中第一次出现的位置
     *
     * @param t
     * @return
     */
    public int indexOf(T t) {
        Node<T> item = first;
        for (int j = 0; first.next != null; j++) {
            item = first.next;
            if (Objects.equals(item.data, t)) {
                return j;
            }
        }
        return -1;

    }


    /**
     * 遍历实现
     *
     * @return
     */
    @Override
    public Iterator<T> iterator() {
        return new TowWayLinkListIterator();
    }

    private class TowWayLinkListIterator implements Iterator<T> {
        private Node current = first;

        @Override
        public boolean hasNext() {
            return Objects.nonNull(current.next);
        }

        @Override
        public T next() {
            current = current.next;
            return (T) current.data;
        }
    }


}
