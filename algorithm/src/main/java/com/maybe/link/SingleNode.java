package com.maybe.link;


import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName SingleNodeTest
 * @date 2022/8/16 12:53
 * @Description
 */
public class SingleNode<T> {
    public T item;
    public SingleNode next;

    public SingleNode(T item, SingleNode next) {
        this.item = item;
        this.next = next;
    }

    /**
     * 使用快慢指针，找到链表的中间节点
     * 使用快慢指针解决，快指针步长是慢指针两倍，快指针走完，慢指针的位置刚好就是中间值
     * <p>
     * 利用快慢指针，我们把一个链表看成一个跑道，假设a的速度是b的两倍，那么当a跑完全程后，b刚好跑一半，以
     * 此来达到找到中间节点的目的。
     *
     * @return
     */
    public static void getMiddle(SingleNode item) {
        isEmpty(item);
        SingleNode<Integer> quick = item;
        SingleNode<Integer> slow = item;
        while (Objects.nonNull(quick.next)) {
            //判断链表长度，偶数（2个中间值）
            if (Objects.isNull(quick.next.next)) {
                System.out.println("偶数链表的中间值：" + slow.item + "，" + slow.next.item);
                return;
            }
            //奇数（1个中间值）
            quick = quick.next.next;
            slow = slow.next;

            //链表有环的情况，链表有环的话，慢指针终有一天追上快指针。
            if (Objects.equals(quick, slow)) {
                System.out.println("链表中存在环：" + true + "，目前方法无法找到中间值");
                return;
            }
        }

        System.out.println("奇数链表的中间值：" + slow.item);
    }


    /**
     * 使用快慢指针，判断链表是否存在环
     * 链表有环的情况，链表有环的话，慢指针终有一天追上快指针。
     * <p>
     * 使用快慢指针的思想，还是把链表比作一条跑道，链表中有环，那么这条跑道就是一条圆环跑道，在一条圆环跑道
     * 中，两个人有速度差，那么迟早两个人会相遇，只要相遇那么就说明有环。
     */
    public static void checkRing(SingleNode item) {
        isEmpty(item);
        SingleNode<Integer> quick = item;
        SingleNode<Integer> slow = item;
        while (Objects.nonNull(quick.next)) {
            quick = quick.next.next;
            slow = slow.next;
            if (Objects.equals(quick, slow)) {
                System.out.println("链表中存在环：" + true);
                return;
            }
        }
        System.out.println("链表中不存在环：" + false);
    }


    /**
     * 使用快慢指针，寻找链表环的入口结点
     * 链表有环的情况，链表有环的话，慢指针终有一天追上快指针。
     * <p>
     * 当快慢指针相遇时，我们可以判断到链表中有环，这时重新设定一个新指针指向链表的起点，且步长与慢指针一样
     * 为1，则慢指针与“新”指针相遇的地方就是环的入口。
     */
    public static void ringEntrance(SingleNode item) {
        isEmpty(item);
        SingleNode<Integer> quick = item;
        SingleNode<Integer> slow = item;
        //当 快慢指针相遇时，temp从第一个结点开始走，步长与慢指针一致
        SingleNode<Integer> temp = null;
        while (Objects.nonNull(quick.next)) {
            quick = quick.next.next;
            slow = slow.next;

            //快慢指针相遇
            if (Objects.equals(quick, slow)) {
                temp = item;
                //定义完成 临时指针，跳过循环 三个指针一起走
                continue;
            }

            //临时指针步长与慢指针一致
            if (Objects.nonNull(temp)) {
                temp = temp.next;

                //终有一天 临时指针会与慢指针相遇，相遇结点就是环的入口
                if (Objects.equals(temp, slow)) {
                    System.out.println("链表中环的入口为：" + temp.item);
                    return;
                }
            }
        }
    }


    /**
     * 循环链表问题（约瑟夫问题）
     * 解题思路：
     * 1.构建含有41个结点的单向循环链表，分别存储1~41的值，分别代表这41个人；
     * 2.使用计数器count，记录当前报数的值；
     * 3.遍历链表，每循环一次，count++；
     * 4.判断count的值，如果是3，则从链表中删除这个结点并打印结点的值
     * （删除结点将当前结点的上一个结点指向当前下一个即可）把count重置为 0；
     *
     * @param item
     */
    public static void CircularLinkedList(SingleNode item) {
        int count = 0;
        //当前结点
        SingleNode current = item;
        //当前上一个结点
        SingleNode before = null;
        //当前结点下一个等于自己时候，所以形成了自环现象（自己指向自己）
        while (current != current.next) {
            count++;
            if (count % 3 == 0) {
                System.out.print(current.item + "，");
                //删除当前结点
                before.next = current.next;
                current = current.next;
            } else {
                before = current;
                current = current.next;
            }
        }
        System.out.print(current.item + "");
    }


    public static void isEmpty(SingleNode item) {
        if (Objects.isNull(item)) {
            throw new RuntimeException("item node is null");
        }
    }


}








