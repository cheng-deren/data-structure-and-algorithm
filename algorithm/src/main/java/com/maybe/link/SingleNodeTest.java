package com.maybe.link;


/**
 * @author cdr
 * @version 1.0
 * @ClassName SingleNodeTest
 * @date 2022/8/16 22:58
 * @Description
 */
@SuppressWarnings(value = "ALL")
public class SingleNodeTest {

    public static void main(String[] args) {
        SingleNode<Integer> node8 = new SingleNode<Integer>(8, null);
        SingleNode<Integer> node7 = new SingleNode<Integer>(7, node8);
        SingleNode<Integer> node6 = new SingleNode<Integer>(6, node7);
        SingleNode<Integer> node5 = new SingleNode<Integer>(5, node6);
        SingleNode<Integer> node4 = new SingleNode<Integer>(4, node5);
        SingleNode<Integer> node3 = new SingleNode<Integer>(3, node4);
        SingleNode<Integer> node2 = new SingleNode<Integer>(2, node3);
        SingleNode<Integer> node1 = new SingleNode<Integer>(1, node2);

        //创造环
        node8.next = node4;

        //使用快慢指针，找到链表的中间节点
        SingleNode.checkRing(node1);
        System.out.println("------------------------");

        //使用快慢指针，判断链表是否存在环
        SingleNode.getMiddle(node1);
        System.out.println("------------------------");

        //使用快慢指针，寻找链表环的入口结点
        SingleNode.ringEntrance(node1);
        System.out.println("------------------------");


        //获取循环链表
        SingleNode node = makeCircularLinkedList();
        //最后 剩余 16，31， 因为 41 % 3 = 2，剩 2 人（约瑟夫和他朋友他俩就没必要继续玩了）
        SingleNode.CircularLinkedList(node);
        System.out.println("");
        System.out.println("------------------------");


    }


    /**
     * CES 约瑟夫问题
     * 构建含有41个结点的单向循环链表，分别存储1~41的值，分别代表这41个人；
     */
    public static  SingleNode makeCircularLinkedList() {
        SingleNode head = null;
        SingleNode current = null;
        for (int i = 1; i <= 41; i++) {
            //第一个结点
            if (i == 1) {
                head = new SingleNode(i, null);
                current = head;
                continue;
            }
            SingleNode<Integer> newNode = new SingleNode<>(i, null);
            current.next = newNode;
            current = newNode;

            //最后一个结点
            if (i == 41) {
                //指向头结点，形成循环链表
                current.next = head;
            }
        }
        return head;
    }
}
