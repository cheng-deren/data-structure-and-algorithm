package com.maybe.link;

import java.util.Iterator;
import java.util.Objects;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyLinkList
 * @date 2022/8/14 16:10
 * @Description 单向链表
 * 单向链表是链表的一种，它由多个结点组成，每个结点都由一个数据域和一个指针域组成，数据域用来存储数据，
 * 指针域用来指向其后继结点。链表的头结点的数据域不存储数据，指针域指向第一个真正存储数据的结点
 */
public class MyLinkList<T> implements Iterable<T> {

    public Node head;
    public int size;


    /**
     * 结点实现
     */
    private class Node<T> {
        public T data;
        public Node next;

        public Node(T data, Node next) {
            this.data = data;
            this.next = next;
        }
    }

    /**
     * 链表初始化
     */
    public MyLinkList() {
        head = new Node<T>(null, null);
        size = 0;
    }


    /**
     * 校验下标
     */
    private void check(int index) {
        if (index >= size || index < 0) {
            throw new RuntimeException("下标越界");
        }
    }

    public void clear() {
        head.next = null;
        size = 0;
    }

    public boolean isEmpty() {
        return size == 0;
    }

    public int length() {
        return size;
    }

    /**
     * 读取并返回线性表中的第i个元素的值
     *
     * @param i
     * @return
     */
    public T get(int i) {
        check(i);
        Node<T> item = head.next;
        for (int j = 0; j < i; j++) {
            item = item.next;
        }
        return item.data;
    }

    /**
     * 往线性表中添加一个元素
     *
     * @param t
     */
    public void insert(T t) {

        Node<T> item = head;
        while (Objects.nonNull(item.next)) {
            item = item.next;
        }
        Node<T> temp = new Node<>(t, null);
        item.next = temp;

        size++;
    }

    /**
     * 向指定位置i处，添加元素t
     */
    public void insert(int i, T t) {
        check(i);
        Node<T> before = head;
        boolean isLast = false;
        //找到 i 下标，前一个结点
        for (int j = 0; j < i; j++) {
            before = before.next;
        }
        //当前（i）结点
        Node<T> current = before.next;
        //创建新结点（尾结点绑定 i 结点）
        Node<T> tNode = new Node<>(t, current);
        before.next = tNode;

        size++;
    }

    /**
     * 删除指定位置i处的元素，并返回被删除的元素
     *
     * @param i
     * @return
     */
    public T remove(int i) {
        check(i);
        Node<T> before = head;
        //找到 i 前面一个结点
        for (int j = 0; j < i; j++) {
            before = before.next;
        }
        //当前（i）结点
        Node<T> current = before.next;

        //删除当前 i 结点
        Node<T> after = current.next;
        before.next = after;
        size--;

        //返回删除的元素
        return current.data;
    }

    /**
     * 查找元素t在链表中第一次出现的位置
     *
     * @param t
     * @return
     */
    public int indexOf(T t) {
        Node<T> item = head;
        int num = 0;
        while (Objects.nonNull(head.next)) {
            num++;
            item = head.next;
            if (Objects.equals(item.data, t)) {
                return num;
            }
        }
        return -1;
    }


    /**
     * 单向链表实现反转
     */
    public void reverse() {
        if (isEmpty()) {
            return;
        }
        //头结点不进行反转。
        reverseRecursion(head.next);
    }

    private Node<T> reverseRecursion(Node<T> current) {
        if (Objects.isNull(current.next)) {
            current.next = null;
            head.next = current;
            return current;
        }
        Node before = reverseRecursion(current.next);
        before.next = current;
        current.next = null;
        return current;
    }


    /**
     * 遍历实现
     *
     * @return
     */
    @Override
    public Iterator iterator() {
        return new MyLinkListIterator();
    }


    private class MyLinkListIterator implements Iterator<T> {
        public Node<T> current;

        public MyLinkListIterator() {
            this.current = head;
        }

        @Override
        public boolean hasNext() {
            return Objects.nonNull(current.next);
        }

        @Override
        public T next() {
            current = current.next;
            return current.data;
        }
    }

}
