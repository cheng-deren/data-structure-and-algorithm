package com.maybe.heap;

import java.util.StringJoiner;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyHeapTest
 * @date 2022/10/17 15:21
 * @Description
 */
public class MyHeapTest {
    public static void main(String[] args) throws Exception {
        MyHeap<String> heap = new MyHeap<String>(10);
        heap.insert("A");
        heap.insert("B");
        heap.insert("C");
        heap.insert("D");
        heap.insert("E");
        heap.insert("F");
        heap.insert("G");

        System.out.println(heap.getSize());

        StringJoiner joiner = new StringJoiner(",");
        while (heap.getSize() > 0) {
            String s = heap.deleteMax();
            joiner.add(s);
        }
        System.out.println(joiner.toString());
    }
}
