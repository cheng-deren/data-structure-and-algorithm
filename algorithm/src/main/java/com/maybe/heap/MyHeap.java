package com.maybe.heap;

/**
 * @author cdr
 * @version 1.0
 * @ClassName MyHeap
 * @date 2022/10/17 10:52
 * @Description 堆 API
 */
public class MyHeap<T extends Comparable<T>> {

    private T[] element;
    private int size;

    public MyHeap(int capacity) {
        this.element = (T[]) new Comparable[capacity + 1];
        this.size = 0;
    }

    /**
     * 判断堆中索引i处的元素是否小于索引j处的元素
     *
     * @param i
     * @param j
     * @return
     */
    private boolean less(int i, int j) {
        return element[i].compareTo(element[j]) < 0;
    }

    /**
     * 交换堆中i索引和j索引处的值
     *
     * @param i
     * @param j
     */
    private void exchange(int i, int j) {
        T temp = element[i];
        element[i] = element[j];
        element[j] = temp;
    }

    public int getSize() {
       return size;
    }

    /**
     * 往堆中插入一个元素
     *
     * @param t
     */
    public void insert(T t) {
        //堆第一个位置不存储元素，下标从0开始
        element[++size] = t;
        //size = 最后一个元素，也就是刚刚插入进来的元素
        //使用上浮算法，调整堆的有序性
        ascent(size);
    }

    /**
     * 使用上浮算法，使索引k处的元素能在堆中处于一个正确的位置
     * 父节点：   n / 2
     * 左子节点： 2n
     * 右子节点： 2n+1
     */
    private void ascent(int current) {
        //拿当前位置的元素 与 父节点比较，父节点比current元素小则交换
        while (current > 1) {
            if (less(current / 2, current)) {
                exchange(current / 2, current);
            }
            //跟新下标，遍历一次往上找一层
            current = current / 2;
        }
    }

    /**
     * 删除堆中最大的元素,并返回这个最大元素
     *
     * @return
     */
    public T deleteMax() {
        T max = element[1];
        //1.需要先将最大结点 与 最小结点 交换位置
        exchange(1, size);
        //2.删除最后位置上的元素 （最大元素）
        element[size] = null;
        size--;
        downward(1);
        return max;
    }

    /**
     * 使用下沉算法，使索引current处的元素能在堆中处于一个正确的位置
     *
     * @param current
     */
    private void downward(int current) {
        //从父节点开始往下遍历
        int left = 2 * current;
        int right = 2 * current + 1;
        while (left <= size) {
            //当前层级最大的节点
            int max;
            //右节点不为空
            if (right <= size) {
                //左右节点比较大小
                if (less(left, right)) {
                    max = right;
                } else {
                    max = left;
                }
            } else {
                max = left;
            }
            //拿当前节点 和 max 比较
            if (less(max, current)) {
                //当前节点，大于 max（该层最大元素）则结束循环
                break;
            }

            //当前节点小于 max 则交换元素
            exchange(max, current);

            //跟新下标，遍历一次往下找一层
            left = max;
        }
    }

}
